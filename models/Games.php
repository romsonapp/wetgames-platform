<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "games".
 *
 * @property integer $game_id
 * @property string $game_title
 * @property string $game_description
 * @property integer $game_start
 * @property integer $game_stop
 * @property \app\models\Gameplay $gameplay
 * @property mixed $authors
 * @property integer $gameplay_id
 * @property integer $fee
 * @property integer $invites_available
 * @property integer $online_statistic
 * @property integer $status_id
 */
class Games extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'games';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_title', 'game_description', 'game_start', 'game_stop'], 'required'],
            [['game_description'], 'string'],
            [['gameplay_id'], 'string'],
            [['fee', 'invites_available', 'online_statistic', 'status_id'], 'integer'],
            [['game_title'], 'string', 'max' => 255],
        ];
    }

    public function newGame($data)
    {
        $this->load($data);

        if ($this->save()) {
            $id = Yii::$app->db->getLastInsertID();
            $gameplay = new Gameplay();
            $gameplay->setGameplayId($id);
            $monitor = new Monitor();
            $monitor->setMonitorId($id);
            if ($gameplay->newGameplay() && $monitor->newMonitor()) {
                $this->gameplay_id = 'gameplay_' . $id;
                $this->type_id = GameType::WET_WARS;
                $this->update();
                if (GamesAuthors::addAuthor($this->game_id, Yii::$app->getUser()->id)) {
                    return true;
                }
            } else {
                Games::findOne(['game_id' => $id])->delete();
            }
        }
        return false;
    }

    public function getAuthors()
    {
        return $this->hasMany(GamesAuthors::className(), ['game_id' => 'game_id']);
    }

    public function isStarted()
    {
        return (time() - strtotime($this->game_start)) >= 0 ? true : false;
    }

    public static function closeGame($game_id)
    {
        $game = self::find()->where(['game_id' => $game_id])->one();
        $game->status_id = GameStatus::CLOSED;
        $game->update();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'game_id' => 'ID',
            'game_title' => 'Название игры',
            'game_description' => 'Описание игры',
            'game_start' => 'Начало',
            'game_stop' => 'Конец',
            'gameplay_id' => 'Gameplay ID',
            'fee' => 'Взнос',
            'online_statistic' => 'Доступность статистики для игроков во время игры',

        ];
    }

    public function beforeSave($insert)
    {
        $this->game_start = is_int($this->game_start) ? $this->game_start : strtotime($this->game_start);
        $this->game_stop = is_int($this->game_stop) ? $this->game_stop : strtotime($this->game_stop);


        return parent::beforeSave($insert);
    }


    public function afterFind()
    {
        parent::afterFind();
        $this->game_start = date('m/d/Y H:i', $this->game_start);
        $this->game_stop = date('m/d/Y H:i', $this->game_stop);
    }

    /**
     * @param $user_id
     */
    public function invite($user_id)
    {
        $gameplay = $this->getGameplay();
        $gameplay->status = Gameplay::PLAYER_INVITED;
        $gameplay->user_id = $user_id;

        if (!$gameplay::find()->where(['user_id' => $user_id])->exists()) {
            $gameplay->save();
        }
    }

    /**
     * @param $user_id
     */
    public function cancelInvite($user_id)
    {
        if ($gameplay = $this->getGameplay()->find()->where(['user_id' => $user_id])->one()) {
            $gameplay->delete();
        }
    }


    public function isClosed()
    {
        return $this->status_id == GameStatus::CLOSED ? true : false;
    }

    public function getStartTimeTimestamp()
    {
        return strtotime($this->game_start);
    }

    public function getStopTimeTimestamp()
    {
        return strtotime($this->game_stop);
    }

    public function canPlayerEntry()
    {
        return ($this->getStartTimeTimestamp() - time()) <= 300 && !$this->isClosed() && $this->isInvited() ? true : false;
    }

    public function hasTarget() {
        $player =  $this->getGameplay()->find()->where(['user_id' => Yii::$app->user->identity->getId()])->select(['target_id'])->one();

        if(empty($player->target_id)) {
            return false;
        }
        return true;
    }

    /**
     * @return Gameplay
     */
    public function getGameplay()
    {
        $gameplay = new Gameplay();
        $gameplay->setGameplayId($this->gameplay_id);
        return $gameplay;
    }

    public function isInvited()
    {
        return $this->getGameplay()->find()->where(['status' => Gameplay::PLAYER_ACTIVE, 'user_id' => Yii::$app->user->identity->getId()])->exists() ? true : false;
    }

    public function checkInvite()
    {
        foreach ($this->getGameplay()->getAllPlayers() as $user) {
            if ($user->user_id == Yii::$app->user->identity->id)
                return true;
        }

        return false;
    }

    public function getStatus()
    {
        return $this->hasOne(GameStatus::className(), ['id' => 'status_id']);
    }

    public function getType()
    {
        return $this->hasOne(GameType::className(), ['id' => 'type_id']);
    }

    public function isStatisticOpen()
    {
        return $this->online_statistic == 1 ? true : false;
    }


    public function isUserAuthor()
    {
        $authors = [];
        foreach ($this->authors as $author) {
            $authors[] = $author->user_id;
        }

        return in_array(Yii::$app->user->identity->getId(), $authors) ? true : false;
    }
}
