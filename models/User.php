<?php

namespace app\models;

use app\modules\admin\models\user\AuthAssignment;
use dektrium\rbac\models\Assignment;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends \dektrium\user\models\User
{
    const ACCESS_AUTHOR = 'author';

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        // add field to scenarios
        $scenarios['create'][]   = 'phone';
        $scenarios['update'][]   = 'phone';
        $scenarios['register'][] = 'phone';
        return $scenarios;
    }

    public function rules()
    {
        $rules = parent::rules();
        // add some rules
        $rules['fieldRequired'] = ['phone', 'required'];
        $rules['fieldLength']   = ['phone', 'string', 'max' => 14];

        return $rules;
    }

    public function checkProfile()
    {
        $attributes = [
            $this->profile->name,
            $this->profile->lastname,
            $this->profile->address,
            $this->profile->schedule,
            $this->profile->photo,

        ];

        foreach ($attributes as $attribute) {
            if(empty($attribute)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return bool Whether the user is an admin or not.
     */
    public function getIsAuthor()
    {
        return
            (\Yii::$app->getAuthManager() && $this->module->adminPermission ?
                \Yii::$app->user->can($this->module->adminPermission) : false)
            || in_array($this->username, $this->module->admins);
    }

    public function getRoles() {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }
    public function isAuthor() {
        $roles = [];

        foreach ($this->roles as $role) {
            $roles[] = $role->item_name;
        }

        return in_array(User::ACCESS_AUTHOR, $roles) ? true : false;

    }


}
