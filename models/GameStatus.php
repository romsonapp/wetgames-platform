<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "game_status".
 *
 * @property integer $id
 * @property string $label
 * @property string $icon
 */
class GameStatus extends \yii\db\ActiveRecord
{
    const OPEN = 1;
    const CLOSED = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['label'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Статус игры',
        ];
    }
}
