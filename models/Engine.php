<?php
namespace app\models;

class Engine
{


    public static function generateLifecodes($size)
    {
        $x = '';

        //$str = "qwertyuiopasdfghjklzxcvbnm123456789";
        $str = "0123456789";

        for ($i = 0; $i < $size; $i++) {
            $x .= substr($str, mt_rand(0, strlen($str) - 1), 1);
        }

        return $x;
    }


    public static function timeDiff($t1, $t2) {
        $diff = $t1 - $t2;
        $days = floor($diff / 86400);
        $hours = floor(($diff % 86400) / 3600);
        $minutes = floor(($diff % 3600) / 60);
        $second = $diff - ( $days * 86400 + $hours * 3600 + $minutes * 60 );
        return compact('days', 'hours', 'minutes', 'second');
    }



    /**
     * Счетчик обратного отсчета
     *
     * @param mixed $date
     * @return
     */
    public static function downcounter($date)
    {
        $check_time = strtotime($date) - time();
        if ($check_time <= 0) {
            return false;
        }

        $days = floor($check_time / 86400);
        $hours = floor(($check_time % 86400) / 3600);
        $minutes = floor(($check_time % 3600) / 60);
        $seconds = $check_time % 60;

        $str = '';
        if ($days > 0) $str .= self::declension($days, array('день', 'дня', 'дней')) . ' ';
        if ($hours > 0) $str .= self::declension($hours, array('час', 'часа', 'часов')) . ' ';
        if ($minutes > 0) $str .= self::declension($minutes, array('минута', 'минуты', 'минут')) . ' ';
        if ($seconds > 0) $str .= self::declension($seconds, array('секунда', 'секунды', 'секунд'));

        return $str;
    }


    /**
     * Функция склонения слов
     *
     * @param mixed $digit
     * @param mixed $expr
     * @param bool $onlyword
     * @return
     */
    private static function declension($digit, $expr, $onlyword = false)
    {
        if (!is_array($expr)) $expr = array_filter(explode(' ', $expr));
        if (empty($expr[2])) $expr[2] = $expr[1];
        $i = preg_replace('/[^0-9]+/s', '', $digit) % 100;
        if ($onlyword) $digit = '';
        if ($i >= 5 && $i <= 20) $res = $digit . ' ' . $expr[2];
        else {
            $i %= 10;
            if ($i == 1) $res = $digit . ' ' . $expr[0];
            elseif ($i >= 2 && $i <= 4) $res = $digit . ' ' . $expr[1];
            else $res = $digit . ' ' . $expr[2];
        }
        return trim($res);
    }
}