<?php
namespace app\models\user;

class RegistrationForm extends \dektrium\user\models\RegistrationForm {
    /**
     * @var string
     */
    public $phone;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['fieldRequired'] = ['phone', 'required'];
        $rules['fieldLength']   = ['phone', 'string', 'max' => 13];
        return $rules;
    }
}

