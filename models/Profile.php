<?php
namespace app\models;

/**
 * @property string $lastname
 *
 **/

class Profile extends \dektrium\user\models\Profile
{


    public function rules()
    {
        $rules = parent::rules();

        return $rules;
    }

    public function scenarios()
    {
        $scenarios =  parent::scenarios(); // TODO: Change the autogenerated stub
        $scenarios['default'][] = 'middlename';
        $scenarios['default'][] = 'lastname';
        $scenarios['default'][] = 'gender';
        $scenarios['default'][] = 'birthday_date';
        $scenarios['default'][] = 'birthday_year';
        $scenarios['default'][] = 'city';
        $scenarios['default'][] = 'work_address';
        $scenarios['default'][] = 'study_address';
        $scenarios['default'][] = 'address';
        $scenarios['default'][] = 'schedule';
        $scenarios['default'][] = 'transport_mark';
        $scenarios['default'][] = 'transport_model';
        $scenarios['default'][] = 'transport_name';
        $scenarios['default'][] = 'is_public_contacts';

        return $scenarios;
    }

    /**
     * Returns avatar url or null if avatar is not set.
     * @param  int $size
     * @return string|null
     */
    public function getAvatarUrl($size = 200)
    {
        return file_exists($this->photo) ? DIRECTORY_SEPARATOR . $this->photo : '/uploads/icons/no-photo.jpeg';
    }


}