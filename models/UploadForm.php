<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $photo;

    public function rules()
    {
        return [
            [['photo'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $file = 'uploads/' . 'photo_user_' . \Yii::$app->getUser()->id . '.' . $this->photo->extension;
            $this->photo->saveAs($file);
            return $file;
        } else {
            return false;
        }
    }
}