<?php
namespace app\models;

use dektrium\user\models\User as BaseUser;
use Yii;


/**
 * This is the model class for tables "gameplay".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $target_id
 * @property string $lifecode
 * @property array|\yii\db\ActiveRecord[] $allPlayers
 * @property \yii\db\ActiveQuery $target
 * @property mixed $gameplayId
 * @property \yii\db\ActiveQuery $user
 * @property array $targetsMap
 * @property integer $status
 */
class Gameplay extends \yii\db\ActiveRecord
{
    const PLAYER_ACTIVE = 1;
    const PLAYER_INACTIVE = 2;
    const PLAYER_INVITED = 3;

    private static $gameplay_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return self::$gameplay_id;
    }

    public function getPlayersArray()
    {
        $players = [];
        foreach ($this->getAllActivePlayers() as $player) {
            $players[$player->user_id] = $player->player->username;
        }

        return $players;

    }

    public function getAllLifecodes()
    {
        $data = [];
        $rows = self::find()->all();

        foreach ($rows as $row) {
            $data[] = $row;
        }
        return $data;
    }

    public function getPlayer()
    {
        return $this->hasOne(BaseUser::className(), ['id' => 'user_id']);
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['lifecode'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'target_id' => 'Target ID',
            'lifecode' => 'Lifecode',
            'status' => 'Status',
        ];
    }

    /**
     * @return bool
     */
    public function checkWinner()
    {
        if (self::find()->where(['status' => self::PLAYER_ACTIVE])->count() > 1)
            return false;
        return true;
    }

    public function getFormattedLifecode()
    {
        return implode('-', str_split($this->lifecode, 3));
    }

    /**
     * @return bool|int
     */
    public function generateTargets()
    {
        $players = [];
        $lifecodes = [];
        $gameplays = $this->getAllActiveAndInvitedPlayers();


        $players_count = count($gameplays);

        do {
            $lcode = Engine::generateLifecodes(6);
            if (!in_array($lcode, $lifecodes)) {
                $lifecodes[] = $lcode;
            }
        } while (count($lifecodes) < $players_count);
        shuffle($gameplays);

        for ($i = 0; $i < $players_count; $i++) {
            if (($i + 1) < $players_count) {
                $target = $gameplays[$i + 1];
                $gameplays[$i]->lifecode = $lifecodes[$i];
                $gameplays[$i]->target_id = $target->user_id;
                $gameplays[$i]->status = self::PLAYER_ACTIVE;

                //$gameplays[$i]->load(['lifecode' => $lifecodes[$i], 'target_id' => $target->user_id, 'status' => 1]);

            } else {
                $gameplays[$i]->lifecode = $lifecodes[$i];
                $gameplays[$i]->target_id = $gameplays[0]->user_id;
                $gameplays[$i]->status = self::PLAYER_ACTIVE;
                //$gameplays[$i]->load(['lifecode' => $lifecodes[$i], 'target_id' => $gameplays[0]->user_id, 'status' => 1]);
            }

            $players[] = $gameplays[$i];
        }


        foreach ($players as $player) {
            if ($player->update()) {
                $players_count--;
            }
        }

        if ($players_count === 0)
            return true;


        return $players_count;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getAllPlayers()
    {
        return self::find()->all();
    }

    private function getAllActiveAndInvitedPlayers()
    {
        return self::find()->where(['status' => [self::PLAYER_ACTIVE, self::PLAYER_INACTIVE]])->all();
    }

    private function getAllActivePlayers()
    {
        return self::find()->where(['status' => [self::PLAYER_ACTIVE]])->all();
    }


    /**
     * @param $game_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getInvitedPlayers($game_id)
    {
        self::setGameplayId($game_id);
        return self::find()->where(['status' => self::PLAYER_INVITED])->all();
    }

    public static function getPlayedPlayers($game_id)
    {
        self::setGameplayId($game_id);
        return self::find()->where(['status' => [self::PLAYER_ACTIVE, self::PLAYER_INACTIVE]])->all();
    }

    public static function getInActivePlayers($game_id)
    {
        self::setGameplayId($game_id);
        return self::find()->where(['status' => self::PLAYER_INACTIVE])->all();
    }

    /**
     * @param $game_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getActivePlayers($game_id)
    {
        self::setGameplayId($game_id);
        return self::find()->where(['status' => self::PLAYER_ACTIVE])->all();
    }

    /**
     * @return array
     */
    public function getTargetsMap()
    {
        $map = [];
        $players = self::getAllActivePlayers();
        $players_count = count($players);

        if (!$players || !$players[0]->target_id) {
            return false;
        }


        $tmp_map = [];
        for ($i = 0; $i < $players_count; $i++) {

            $tmp_map[$players[$i]->user_id] = [
                'isLast' => false,
                'target' => $players[$i]->target ? $players[$i]->target->username : null,
                'target_id' => $players[$i]->target ? $players[$i]->target->id : null,
            ];
        }


        $first_id = key($tmp_map);

        $map[0] = $tmp_map[$first_id];

        for ($i = 0; $i < $players_count - 1; $i++) {
            if (!isset($tmp_map[$map[$i]['target_id']])) {
                return false;
            }
            $map[] = $tmp_map[$map[$i]['target_id']];
        }


        $tmp = [];
        $tmp_map = [];

        array_filter($map, function ($item) use (&$tmp, &$tmp_map) {
            if (!in_array($item['target_id'], $tmp)) {
                $tmp[] = $item['target_id'];
                $tmp_map[] = $item;
            }
        });

        $map = $tmp_map;

        if ($players_count != count($map)) {
            return false;
        }


        $map[count($map) - 1]['isLast'] = true;

        return $map;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(BaseUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarget()
    {
        return $this->hasOne(BaseUser::className(), ['id' => 'target_id']);
    }


    /**
     * @return bool
     */
    public function kill()
    {
        $this->status = self::PLAYER_INACTIVE;

        $monitor = new Monitor();
        $monitor->setMonitorId(self::$gameplay_id);

        if ($this->update() && $monitor->log(Yii::$app->user->identity->getId(), $this->user_id, $this->lifecode)) {
            return true;
        }
        return false;
    }


    public function setPlayerInactive($user_id)
    {
        $player = self::find()->where(['user_id' => $user_id])->one();
        $player->status = self::PLAYER_INVITED;
        $player->target_id = null;
        $player->update();
    }


    public function setPlayerActive($user_id)
    {
        $player = self::find()->where(['user_id' => $user_id])->one();
        $player->status = self::PLAYER_ACTIVE;
        $player->update();
    }


    /**
     * @param $game_id
     */
    public static function setGameplayId($game_id)
    {
        self::$gameplay_id = $game_id;
    }


    /**
     * @param $game_id
     * @return bool
     */
    public function newGameplay()
    {
        $game_id = self::$gameplay_id;
        $sql = "CREATE TABLE IF NOT EXISTS `gameplay_{$game_id}` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `user_id` int(11) NOT NULL,
              `target_id` int(11) NULL,
              `lifecode` VARCHAR (50) NULL,
              `status` int(11) NOT NULL DEFAULT 1,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB  DEFAULT CHARSET='utf8' COMMENT='Gameplay_{$game_id} Table' AUTO_INCREMENT=1 ;";


        $q = self::getDb()->createCommand($sql);
        $q->execute();

        return self::getDb()->schema->getTableSchema('gameplay_' . $game_id) === null ? false : true;
    }

    public static function getDb()
    {
        return Yii::$app->wet_wars_games;
    }

}