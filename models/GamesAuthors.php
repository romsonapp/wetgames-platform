<?php

namespace app\models;

use dektrium\user\models\Profile as BaseProfile;
use dektrium\user\models\User as BaseUser;
use yii\helpers\Html;

/**
 * This is the model class for table "games_authors".
 *
 * @property integer $id
 * @property integer $game_id
 * @property integer $user_id
 */
class GamesAuthors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'games_authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'user_id'], 'required'],
            [['game_id', 'user_id'], 'integer'],
        ];
    }

    public function getProfile() {
        return $this->hasOne(BaseProfile::className(), ['user_id' => 'user_id']);
    }

    public function getUser() {
        return $this->hasOne(BaseUser::className(), ['id' => 'user_id']);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'user_id' => 'User ID',
        ];
    }

    public static function getAuthorsToString(Games $game) {
        $authors = self::find()->where(['game_id' => $game->game_id])->all();
        $authorsArr = [];
        foreach ($authors as $author) {
            $authorsArr[] = Html::a($author->user->username, ['#']);
        }
        return implode(', ', $authorsArr);
    }

    public static function addAuthor($game_id, $user_id) {
        $model = new self();
        $model->game_id = $game_id;
        $model->user_id = $user_id;

        if ($model->save())
            return true;

        return false;
    }
}

