<?php
namespace app\models;

use dektrium\user\models\User as BaseUser;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for tables "monitor".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $target_id
 * @property integer $kill_time
 * @property string $lifecode
 */
class Monitor extends ActiveRecord
{
    private static $monitor_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return self::$monitor_id;
    }

    public function getUser()
    {
        return $this->hasOne(BaseUser::className(), ['id' => 'user_id']);
    }

    public function getTarget()
    {
        return $this->hasOne(BaseUser::className(), ['id' => 'target_id']);
    }

    /**
     * @param $game_id
     */
    public static function setMonitorId($gameplay_id)
    {
        self::$monitor_id = $gameplay_id;
    }

    public function log($user_id, $target_id, $lifecode)
    {
        $this->user_id = $user_id;
        $this->target_id = $target_id;
        $this->lifecode = $lifecode;
        $this->kill_time = microtime(true);
        if ($this->save())
            return true;

        return false;
    }


    public static function getStatisticData($gameplay_id)
    {

        self::setMonitorId($gameplay_id);
        $game = Games::find()->where(['gameplay_id' => $gameplay_id])->one();
        $rows = self::find()->all();

        if (!$rows)
            return false;
        $data = [];

        for ($i = 0; $i < count($rows); $i++) {

            $row = $rows[$i];
            $micro = sprintf("%03d",($row->kill_time - floor($row->kill_time)) * 1000);
            $data[$row->user_id]['kills'][$row->kill_time]['target_id'] = $row->target_id;
            $data[$row->user_id]['kills'][$row->kill_time]['print']['name'] = $row->target->username;
            $data[$row->user_id]['kills'][$row->kill_time]['print']['date'] = date('d/m/Y', $row->kill_time);
            //$data[$row->user_id]['kills'][$row->kill_time]['print']['time'] = date('H:i:s.', $row->kill_time);
            $data[$row->user_id]['kills'][$row->kill_time]['print']['time_micro'] = $micro;

            $prevTime = strtotime($game->game_start);

          //  var_dump($prevRow = prev($rows));
            if(isset($rows[$i - 1]) && $prevRow = $rows[$i - 1]) {
                $prevTime = $prevRow->kill_time;
            }

            $times = Engine::timeDiff($row->kill_time, $prevTime);

            if($times['days'] > 0) {
                $time = sprintf('%2d д. %2d ч.', $times['days'], $times['hours']);
            } else {
                if($times['hours'] > 0) {
                    $time = sprintf('%2d ч. %2d м.', $times['hours'], $times['minutes']);
                } else {
                    if($times['minutes'] > 0) {
                        $time = sprintf('%2d м. %2d с.', $times['minutes'], $times['second']);
                    } else {
                        $time = sprintf('%2d с.', $times['second']);
                    }
                }
            }


            $data[$row->user_id]['kills'][$row->kill_time]['print']['time'] = date('H:i:s.', $row->kill_time);

            $data[$row->user_id]['kills'][$row->kill_time]['print']['time_after_start'] = $time;

            $data[$row->user_id]['print'] = $row->user->username;

            //$data[$row->user_id]['death'][$row->kill_time]['target_id'] = $row->target_id;
            if (isset($data[$row->target_id])) {
                $data[$row->target_id]['death']['target_id'] = $row->user_id;
                $data[$row->target_id]['death']['timestamp'] = $row->kill_time;
//                $data[$row->target_id]['death']['target_id'] = $row->user_id;
                $data[$row->target_id]['death']['print']['name'] = $row->user->username;
                $data[$row->target_id]['death']['print']['date'] = date('d/m/Y', $row->kill_time);
                $data[$row->target_id]['death']['print']['time'] = date('H:i:s.', $row->kill_time);
                $data[$row->target_id]['death']['print']['time_micro'] = $micro;

            }



        }



        /*foreach ($rows as $row) {
            $micro = sprintf("%03d",($row->kill_time - floor($row->kill_time)) * 1000);
            $data[$row->user_id]['kills'][$row->kill_time]['target_id'] = $row->target_id;
            $data[$row->user_id]['kills'][$row->kill_time]['print']['name'] = $row->target->username;
            $data[$row->user_id]['kills'][$row->kill_time]['print']['date'] = date('d/m/Y', $row->kill_time);
            $data[$row->user_id]['kills'][$row->kill_time]['print']['time'] = date('H:i:s.', $row->kill_time);
            $data[$row->user_id]['kills'][$row->kill_time]['print']['time_micro'] = $micro;



            $diff = $row->kill_time - strtotime($game->game_start);
            $days = floor($diff / 86400);
            $hours = floor(($diff % 86400) / 3600);
            $minutes = floor(($diff % 3600) / 60);

            if($days > 0) {
                $time = sprintf('%2d д. %2d ч.', $days, $hours);
            } else {
                $time = sprintf('%02d ч. %2d м.', $hours, $minutes);
            }

            $data[$row->user_id]['kills'][$row->kill_time]['print']['time_after_start'] = $time;

            $data[$row->user_id]['print'] = $row->user->username;

            //$data[$row->user_id]['death'][$row->kill_time]['target_id'] = $row->target_id;
            if (isset($data[$row->target_id])) {
                $data[$row->target_id]['death']['target_id'] = $row->user_id;
                $data[$row->target_id]['death']['timestamp'] = $row->kill_time;
//                $data[$row->target_id]['death']['target_id'] = $row->user_id;
                $data[$row->target_id]['death']['print']['name'] = $row->user->username;
                $data[$row->target_id]['death']['print']['date'] = date('d/m/Y', $row->kill_time);
                $data[$row->target_id]['death']['print']['time'] = date('H:i:s.', $row->kill_time);
                $data[$row->target_id]['death']['print']['time_micro'] = $micro;

            }
            //$data[$row->user_id]['time'][] = date('d/m/Y H:i:s',$row->kill_time);
        }*/

        $tmp_data = [];
        $c = 0;
        foreach ($data as $id => $item) {
            $last_kill_timestamp = max(array_keys($item['kills']));

            $times = Engine::timeDiff($last_kill_timestamp, strtotime($game->game_start));

            $tmp_data[$c]['id'] = $id;
            $tmp_data[$c]['data'] = $item;
            $tmp_data[$c]['data']['how_long'] = sprintf('%2d д. %2d ч. %2d м.', $times['days'], $times['hours'], $times['minutes']);
            $c++;
        }

        for ($i = count($tmp_data); $i >= 0; $i--) {
            for ($j = 0; $j < $i - 1; $j++) {
                $tmp_kills = isset($tmp_data[$j]['data']['kills']) ? count($tmp_data[$j]['data']['kills']) : 0;
                $kills = isset($tmp_data[$j + 1]['data']['kills']) ? count($tmp_data[$j + 1]['data']['kills']) : 0;

                if ($kills == $tmp_kills) {

                    $priority = 0;

                    for ($t = 0; $t < $tmp_kills - 1; $t++) {

                        $time1 = max(array_keys($tmp_data[$t]['data']['kills']));

                        $time2 = max(array_keys($tmp_data[$t + 1]['data']['kills']));

                        if ($time2 < $time1) {
                            $priority = 1;
                        }

                    }
                    if ($priority == 1) {
                        $tmp = $tmp_data[$j];
                        $tmp_data[$j] = $tmp_data[$j + 1];
                        $tmp_data[$j + 1] = $tmp;
                    }
                } else if ($kills > $tmp_kills) {
                    $tmp = $tmp_data[$j];
                    $tmp_data[$j] = $tmp_data[$j + 1];
                    $tmp_data[$j + 1] = $tmp;
                }
            }
        }
        $data = [];
        foreach ($tmp_data as $item) {
            $data[$item['id']] = $item['data'];
        }

        return $data;

    }

    /**
     * @param $game_id
     * @return bool
     */
    public function newMonitor()
    {
        $monitor_id = self::$monitor_id;
        $sql = "CREATE TABLE IF NOT EXISTS `gameplay_{$monitor_id}` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `user_id` int(11) NOT NULL,
              `target_id` int(11) NULL,
              `lifecode` VARCHAR (50) NULL,
              `kill_time` DOUBLE NOT NULL DEFAULT 1,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB  DEFAULT CHARSET='utf8' COMMENT='Monitor_{$monitor_id} Table' AUTO_INCREMENT=1 ;";


        $q = self::getDb()->createCommand($sql);
        $q->execute();

        return self::getDb()->schema->getTableSchema('gameplay_' . $monitor_id) === null ? false : true;
    }


    public static function getDb()
    {
        return Yii::$app->wet_wars_monitor;
    }
}