<?php

namespace app\models;

use app\models\Games;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * GamesSearch represents the model behind the search form about `app\models\Games`.
 */
class GamesSearch extends Games
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'game_start', 'game_stop', 'gameplay_id'], 'integer'],
            [['game_title', 'game_description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Games::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'game_id' => $this->game_id,
            'game_start' => $this->game_start,
            'game_stop' => $this->game_stop,
            'gameplay_id' => $this->gameplay_id,
            //'status.search' => $this->game_id,
        ]);

        if (!Yii::$app->user->identity->isAdmin) {
            $query->joinWith('authors');
            $query->andFilterWhere(['games_authors.user_id' => Yii::$app->user->id]);
        }

        $query->andFilterWhere(['like', 'game_title', $this->game_title])
            ->andFilterWhere(['like', 'game_description', $this->game_description]);


        if(Yii::$app->request->get('sort') == 'archive') {
            $query->andFilterWhere(['status_id' => GameStatus::CLOSED]);
        } else {
            $query->andFilterWhere(['status_id' => GameStatus::OPEN]);
        }

        return $dataProvider;
    }
}
