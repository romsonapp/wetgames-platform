<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "game_type".
 *
 * @property integer $id
 * @property string $label
 */
class GameType extends \yii\db\ActiveRecord
{
    const WET_WARS = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['label'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Label',
        ];
    }
}
