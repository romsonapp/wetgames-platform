<?php
namespace app\managers;

use app\exceptions\AccessDenied;
use app\models\GamesAuthors;
use Exception;
use Yii;
use yii\rbac\CheckAccessInterface;
use yii\rbac\Item;
use yii\rbac\Rule;

class RuleManager extends Rule {

    /**
     * Executes the rule.
     *
     * @param string|int $user the user ID. This should be either an integer or a string representing
     * the unique identifier of a user. See [[\yii\web\User::id]].
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to [[CheckAccessInterface::checkAccess()]].
     * @return bool a value indicating whether the rule permits the auth item it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if($item->ruleName === 'game') {
            if($game_id = Yii::$app->request->get('id')) {
                $models = GamesAuthors::find()->select('user_id')->where(['game_id' => $game_id])->all();
                $authors = [];
                foreach ($models as $model)
                    $authors[] = $model->user_id;

                if(!in_array($user, $authors))
                    return false;

            }
        }

        return true;

    }

}