<?php

namespace app\modules\admin;

use app\modules\admin\models\user\AuthAssignment;
use dektrium\user\models\User;
use Yii;
use yii\base\Module as BaseModule;

/**
 * cq-platform-admin module definition class
 */
class Module extends BaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    public $urlPrefix = 'admin';

    public $defaultRoute = 'dashboard/index';

    public $urlRules = [
        'user' => 'user/admin',
        'user/role' => 'rbac/role',
        'user/permission' => 'rbac/permission',
        'user/rule' => 'rbac/rule',
        'user/create' => 'user/admin/create',
        'user/role/create' => 'rbac/role/create',
        'user/permission/create' => 'rbac/permission/create',
        'user/rule/create' => 'rbac/rule/create',

    ];


    /**
     * 11     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        foreach ($this->urlRules as $name => $rule) {
            $configUrlRule = [
                'class' => 'yii\web\UrlRule',
                'name' => $this->urlPrefix . '/' . $name,
                'pattern' => $this->urlPrefix . '/' . $name,
                'route' => $rule,
                'encodeParams' => 1
            ];

            $rule = Yii::createObject($configUrlRule);
            Yii::$app->urlManager->addRules([$rule], false);
        }


    }
}
