<?php
namespace app\modules\admin\controllers;


use app\models\Gameplay;
use app\models\GamesAuthors;
use app\models\User;
use dektrium\user\filters\AccessRule;
use dektrium\user\models\Profile;
use Yii;
use app\models\Games;
use app\models\GamesSearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class GamesController extends Controller {
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['view', 'index', 'create'],
                        'allow' => true,
                        'roles' => ['author']
                    ],

                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Games models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GamesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setSort(['defaultOrder' => ['game_start' => SORT_DESC]]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Games model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $gameplay = new Gameplay();
        $gameplay->setGameplayId($model->gameplay_id);
        $author = new GamesAuthors();

        if(Yii::$app->request->post('GamesAuthors')['user_id']) {
            $author_id = Yii::$app->request->post('GamesAuthors')['user_id'];
            if(!GamesAuthors::find()->where(['game_id' => $id, 'user_id' => $author_id])->exists()) {
                $author->game_id = $id;
                $author->user_id = $author_id;
                if($author->save()) {
                    $this->refresh();
                }
            }
        }

        return $this->render('new_view', [
            'model' => $model,

            'users' => \app\models\Profile::find()->select(['user_id as value', 'name as label'])
                ->asArray()
                ->all(),
            'author' => $author
        ]);
    }

    public function actionClose($game) {
        Games::closeGame($game);
        return $this->redirect(['view', 'id' => $game]);
    }

    public function actionRemoveAuthor($user_id, $game_id) {
        GamesAuthors::deleteAll([
            'user_id' => $user_id,
            'game_id' => $game_id
        ]);

        //$this->redirect(Yii::$app->request->referrer);
        return $this->redirect(['settings', 'id' => $game_id, '#' => 'authors']);
    }

    /**
     * Creates a new Games model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Games();

        if (Yii::$app->request->post()) {
            if ($model->newGame(Yii::$app->request->post())) {
                return $this->redirect(['view', 'id' => $model->game_id]);
            } else {

                return $this->redirect('create', [
                    'model' => $model
                ]);
            }


        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->game_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionGenerateTargets($game)
    {
        $game = Games::find()->where(['game_id' => $game])->one();

        if ($game->getGameplay()->generateTargets()) {
            return $this->redirect(['players-control', 'game' => $game->game_id]);
        }
    }

    /**
     * Updates an existing Games model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $tab = !empty(Yii::$app->request->get('data-action')) ? Yii::$app->request->get('data-action') : '';
            return $this->redirect(['settings', 'id' => $model->game_id, '#' => $tab]);
        } else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    public function actionSettings($id)
    {
        $model = $this->findModel($id);
        $author = new GamesAuthors();

        if(Yii::$app->request->post('GamesAuthors')['user_id']) {
            $author_id = Yii::$app->request->post('GamesAuthors')['user_id'];
            if(!GamesAuthors::find()->where(['game_id' => $id, 'user_id' => $author_id])->exists()) {
                $author->game_id = $id;
                $author->user_id = $author_id;
                if($author->save()) {
                    $this->refresh('#authors');
                }
            }
        }
        return $this->render('game_settings', [
            'model' => $model,
            'author' => $author,
            'users' => \app\models\Profile::find()->select(['user_id as value', 'name as label'])
                ->asArray()
                ->all(),
        ]);
    }



    /**
     * @param $game
     * @return string
     */
    public function actionPlayersControl($game) {
        $model = $this->findModel($game);
        $change_players_model = new \yii\base\DynamicModel(['user1', 'user2']);
        return $this->render('players_control', ['model' => $model,'targets_map' => $model->getGameplay()->getTargetsMap(), 'change_players_model' => $change_players_model]);
    }

    public function actionPlayerSetInactive($game, $user_id) {
        $this->findModel($game)->getGameplay()->setPlayerInactive($user_id);
        return $this->redirect(['players-control', 'game' => $game]);
    }

    public function actionPlayerSetActive($game, $user_id) {
        $this->findModel($game)->getGameplay()->setPlayerActive($user_id);
        return $this->redirect(['players-control', 'game' => $game]);
    }

    public function actionPlayerData($game, $user) {
        $model = $this->findModel($game);

        if($model->getGameplay()->find()->where(['user_id' => $user])->exists() && $model->isUserAuthor()) {
            return $this->render('player-data', [
                'model' => $model,
                'profile' => User::findOne($user)
            ]);
        } else {
            die("У вас нет доступа к просмотру досье этого пользователя.");
        }


    }

    public function actionChangePlayers($id) {
        $data = Yii::$app->request->post('DynamicModel');
        $user1 = $data['user1'];
        $user2 = $data['user2'];

        $game = Games::findOne($id);
        $gameplay = $game->getGameplay();
        $p1 = $gameplay->find()->where(['user_id' => $user1])->one();
        $p2 = $gameplay->find()->where(['user_id' => $user2])->one();

        $p3 = $gameplay->find()->where(['target_id' => $user2])->one();

        $p3->target_id = $p1->user_id;
        $tmp_t = $p2->target_id;
        $p2->target_id = $p1->target_id;

        $p1->target_id = $tmp_t;

        $p1->update();
        $p2->update();
        $p3->update();

        echo '<pre>';
        print_r($p1->errors);
        print_r($p2->errors);
        print_r($p3->errors);
        echo '</pre>';




        return $this->redirect(['players-control', 'game' => $id]);




    }


    /**
     * Deletes an existing Games model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Games model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Games the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Games::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}