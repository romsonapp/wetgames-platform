<?php
namespace app\modules\admin\models\user;

use dektrium\user\models\User;
use yii\db\ActiveRecord;

/**
 *
 * @property mixed $users
 * @property mixed $user
 */
class AuthAssignment extends ActiveRecord   {

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_assignment';
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


}