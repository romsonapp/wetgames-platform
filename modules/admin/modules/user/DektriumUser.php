<?php
namespace app\modules\admin\modules\user;

use app\modules\admin\models\user\AuthAssignment;
use dektrium\user\Module;


class DektriumUser extends Module
{
    const OWNER = 'owner';
    const AUTHOR = 'author';


    public $viewPath = '@app/views/user';
    public $admins = [];
    public function init()
    {
        parent::init();

        foreach (AuthAssignment::find()->where(['item_name' => self::OWNER])->all() as $row) {
            $this->admins[] = $row->user->username;
        }


    }
}