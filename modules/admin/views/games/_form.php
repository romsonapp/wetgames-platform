<?php

use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Games */
/* @var $form yii\widgets\ActiveForm */
$disable = $model->isClosed() ? 'disabled' : false;
$model->online_statistic = $model->online_statistic === NULL ? 0 : $model->online_statistic;

?>
<div class="games-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'game_title')->textInput(['maxlength' => true, 'disabled' => $disable]) ?>

    <?= $form->field($model, 'game_description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6,'disabled' => $disable],
        'preset' => 'basic',

    ]) ?>

    <?= $form->field($model, 'game_start')->textInput(['disabled' => $disable]) ?>

    <?= $form->field($model, 'game_stop')->textInput(['disabled' => $disable]) ?>

    <?= $form->field($model, 'fee')->textInput()->input('number', ['disabled' => $disable]) ?>

    <?= $form->field($model, 'online_statistic')->radioList([1 => 'Включить', 0 => 'Отключить']) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <script>

        $('#games-game_start').daterangepicker({
            "singleDatePicker": true,
            "showWeekNumbers": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "timePickerIncrement": 1,
            "autoApply": true,
            "drops": "up",
            "dateLimit": {
                "days": 7
            },
            "locale": {
                "format": 'MM/DD/YYYY HH:mm'
            }
        });
        $('#games-game_stop').daterangepicker({
            "singleDatePicker": true,
            "showWeekNumbers": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "timePickerIncrement": 15,
            "autoApply": true,
            "drops": "up",
            "dateLimit": {
                "days": 7
            },
            "locale": {
                "format": 'MM/DD/YYYY HH:mm'
            }
        });


    </script>

</div>
