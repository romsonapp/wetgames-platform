<?php use yii\helpers\Html; ?>
<div class="text-center">
    <?= Html::a('Информация об игре', ['view', 'id' => $model->game_id]) ?> |
    <?= Html::a('Настройка игры', ['settings', 'id' => $model->game_id]) ?> |
    <?= Html::a('Управление игроками', ['players-control', 'game' => $model->game_id]) ?> |
    Мониторинг |
    <?= Html::a('Статистика', ['/statistic', 'game' => $model->game_id]) ?> |
    Бонусы/штрафы
    <?php if (!$model->isStarted()): ?>
        | <?= Html::a('Удалить игру', ['delete', 'id' => $model->game_id], [
            'data' => [
                'confirm' => 'Вы уверены в том, что хотите удалить игру?',
                'method' => 'post',
            ],
        ]) ?>
    <?php else: ?>

        <?php if (!$model->isClosed()): ?>
            | <?= Html::a('Закрыть игру', ['close', 'game' => $model->game_id]) ?>

        <?php endif; ?>
    <?php endif; ?>
</div>
<hr>