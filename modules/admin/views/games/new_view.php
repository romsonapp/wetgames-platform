<?php

use yii\helpers\Html;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Games */

$this->title = $model->game_title;

?>


<div class="panel panel-default">
    <div class="panel-heading">
        <h4><?= Html::img($model->type->icon, ['width'=>50]) ?> <?= $model->type->label ?>: (#<?= $model->game_id ?>)
            "<?= $model->game_title ?>"</h4>
    </div>
    <div class="panel-body">
        <?= $this->render('game_menu', compact('model')) ?>

        <div class="text-center">Статус игры: <b><?= !$model->isClosed() ? "Открыта" : "Закрыта" ?></b></div>
        <hr>



    </div>

</div>