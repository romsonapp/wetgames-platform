<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Games */

$this->title = 'Настройки игры: ' . $model->game_title;

?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4><?= Html::img($model->type->icon, ['width'=>50]) ?> <?= $model->type->label ?>: (#<?= $model->game_id ?>)
            "<?= $model->game_title ?>"</h4>
    </div>
    <div class="panel-body">
        <?= $this->render('game_menu', compact('model')) ?>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
</div>
