<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$players = $model->getGameplay()->getPlayersArray();

?>
<style>
    table, th, tr, td {
        border: 1px solid black;
    }

    td, th {
        padding: 10px;
        text-align: center;
    }
</style>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4><?= Html::img($model->type->icon, ['width' => 50]) ?> <?= $model->type->label ?>: (#<?= $model->game_id ?>)
            "<?= $model->game_title ?>"</h4>
    </div>
    <div class="panel-body">
        <?= $this->render('game_menu', compact('model')) ?>
        <div class="col-md-12">

            <?php if (!$model->isClosed()): ?>
                <h3>Схема преследования <?= Html::a('Сгенерировать цели', ['generate-targets', 'game' => $model->game_id], ['class' => 'btn btn-danger']) ?></h3>
                <div class="alert alert-success" role="alert">
                    <?php if (!$targets_map): ?>
                        <div style="color: red;">Необходимо перегенерировать цели.</div>
                    <?php else: ?>
                        <?php foreach ($targets_map as $player):
                            ?>
                            <?= !$player['isLast'] ? $player['target'] . ' -> ' : $player['target'] ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <hr>
            <a href="#" data-toggle="collapse" data-target="#lifecodes" aria-expanded="false" aria-controls="lifecodes">Показать коды жизней</a>

            <div class="collapse" id="lifecodes">
                <div class="well">
                    <table>
                        <thead>
                        <th></th>
                        <th>Участник</th>
                        <th>Код жизни</th>
                        </thead>
                    <?php foreach ($model->getGameplay()->getAllLifecodes() as $i => $lifecode): ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><?= $lifecode->player->username ?></td>
                            <td><?= $lifecode->getFormattedLifecode() ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </table>
                </div>
            </div>
            <hr>
            <div class="col-md-6">

                <ul>
                    <h3>Нужно подтверждение</h3>
                    <?php foreach (\app\models\Gameplay::getInvitedPlayers($model->gameplay_id) as $player): ?>
                        <li style="list-style: none; padding-bottom: 25px;">
                            <h4>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <?= Html::a($player->user->username, ["/admin/games/player-data?game={$model->game_id}&user={$player->user->id}"], ['target' => '_blank']) ?>
                                    </div>
                                    <div class="col-md-1">
                                        <?= Html::a("<div style='color: #00aa00;' class='glyphicon glyphicon-ok'></div>", ['player-set-active', 'game' => $model->game_id, 'user_id' => $player->user_id]) ?>

                                    </div>
                                </div>
                            </h4>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-md-6">
                <ul>
                    <h3>Подтвержденные</h3>
                    <?php foreach (\app\models\Gameplay::getActivePlayers($model->gameplay_id) as $player): ?>
                        <li style="list-style: none; padding-bottom: 25px;">
                            <h4>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <?= Html::a($player->user->username, ["/admin/games/player-data?game={$model->game_id}&user={$player->user->id}"], ['target' => '_blank']   ) ?>
                                    </div>
                                    <div class="col-md-1">
                                        <?= Html::a("<div style='color: #9e0505;' class='glyphicon glyphicon-remove'></div>", ['player-set-inactive', 'game' => $model->game_id, 'user_id' => $player->user_id]) ?>

                                    </div>
                                </div>
                            </h4>
                        </li>
                    <?php endforeach; ?>
                </ul>

            </div>


        </div>
    </div>

</div>