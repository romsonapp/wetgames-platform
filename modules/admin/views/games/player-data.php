<?php
use yii\helpers\Html;
?>
<div class="panel panel-default">
    <div class="panel-body">
        <?= $this->render('game_menu', compact('model')) ?>
        <div class="col-md-12">
            <h3>Личные данные</h3>

            <?php if (!empty($profile->profile->name)): ?>
                <b><?= Html::encode('Имя: ') ?></b><?= Html::encode($profile->profile->name) ?><br>
            <?php endif; ?>
            <?php if (!empty($profile->profile->middlename)): ?>
                <b><?= Html::encode('Отчество: ') ?></b><?= Html::encode($profile->profile->middlename) ?><br>
            <?php endif; ?>
            <?php if (!empty($profile->profile->lastname)): ?>
                <b><?= Html::encode('Фамилия: ') ?></b><?= Html::encode($profile->profile->lastname) ?><br>
            <?php endif; ?>
            <?php if (!empty($profile->profile->gender)): ?>
                <b><?= Html::encode('Пол: ') ?></b><?= Html::encode($profile->profile->gender) ?><br>
            <?php endif; ?>
            <?php if (!empty($profile->profile->birthday_date)): ?>
                <b><?= Html::encode('Дата рождения: ') ?></b><?= Html::encode($profile->profile->birthday_date) ?><br>
            <?php endif; ?>

            <?php if (!empty($profile->profile->birthday_year)): ?>
                <b><?= Html::encode('Год рождения: ') ?></b><?= Html::encode($profile->profile->birthday_year) ?><br>
            <?php endif; ?>
            <hr>
            <h3>Контактные данные</h3>
            <?php if (!empty($profile->phone)): ?>
                <b><?= Html::encode('Мобильный телефон: ') ?></b><?= Html::encode($profile->phone) ?><br>
            <?php endif; ?>
            <hr>

            <h3>Места обитания</h3>
            <br>
            <?php if (!empty($profile->profile->work_address)): ?>
                <b><?= Html::encode('Место и адрес работы:') ?></b>
                <br><?= Html::encode($profile->profile->work_address) ?><br>
            <?php endif; ?>
            <?php if (!empty($profile->profile->study_address)): ?>
                <b><?= Html::encode('Место и адрес учебы:') ?></b>
                <br><?= Html::encode($profile->profile->study_address) ?><br>
            <?php endif; ?>
            <?php if (!empty($profile->profile->address)): ?>
                <b><?= Html::encode('Адрес проживания:') ?></b><br><?= Html::encode($profile->profile->address) ?>
                <br>
            <?php endif; ?>
            <?php if (!empty($profile->profile->schedule)): ?>
                <b><?= Html::encode('Приблизительный распорядок дня:') ?></b>
                <br><?= Html::encode($profile->profile->schedule) ?><br>
            <?php endif; ?>
            <hr>
            <?php if (!empty($profile->profile->transport_mark) || !empty($profile->profile->transport_model) || !empty($profile->profile->transport_name)): ?>
                <h3>Транспорт</h3>
                <?php if (!empty($profile->profile->transport_mark)): ?>
                    <b><?= Html::encode('Марка:') ?></b><br><?= Html::encode($profile->profile->transport_mark) ?>
                    <br>
                <?php endif; ?>
                <?php if (!empty($profile->profile->transport_model)): ?>
                    <b><?= Html::encode('Модель:') ?></b><br><?= Html::encode($profile->profile->transport_model) ?>
                    <br>
                <?php endif; ?>
                <?php if (!empty($profile->profile->transport_name)): ?>
                    <b><?= Html::encode('Гос. номер, имя:') ?></b>
                    <br><?= Html::encode($profile->profile->transport_name) ?><br>
                <?php endif; ?>
                <hr>
            <?php endif; ?>

            <div>
                <?= Html::a(Html::img($profile->profile->getAvatarUrl(), [
                    'class' => 'img-rounded img-responsive',
                    'width' => 200,
                    'style' => 'margin-bottom: 20px;'
                ]), [$profile->profile->getAvatarUrl()]) ?>
            </div>
        </div>
    </div>