<?php

use yii\helpers\Html;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Games */

$this->title = $model->game_title;

?>
<span class="badge"><?= count(\app\models\Gameplay::getInvitedPlayers($model->gameplay_id)) ?></span>
<?= Html::hiddenInput('', $model->game_start, ['id' => 'time-to-start']) ?>
<?= Html::hiddenInput('', $model->game_stop, ['id' => 'time-to-stop']) ?>
<div class="col-md-12">
    <div class="col-md-12 text-center hidden-xs">
        <h4>
            <div style="margin-bottom: 25px;" id="countdown"></div>
        </h4>
    </div>

    <div class="col-sm-8">

        <?= Html::a('Настройки игры', ['update', 'id' => $model->game_id], ['class' => 'btn btn-primary']) ?>
        <?php if (!$model->isClosed()): ?>










        <?php endif; ?>
        <div class="col-md-12 col-md-offset-2"><h1><?= Html::encode($model->game_title) ?><?= $model->isClosed() ? " (<span style='color: red;'>" . Html::encode('игра закрыта') . "</span>)" : ''?></h1></div>
        <?php if ($model->isClosed()): ?>

        <div class="col-md-12 col-md-offset-2"><h3><?= Html::a('статистика', ['/statistic', 'game' => $model->game_id]) ?></h3></div>
        <?php endif; ?>

        <hr>
    </div>

    <?php if (!$model->isClosed()): ?>
    <div class="col-lg-4 hidden-xs">


        <?php $form = ActiveForm::begin(); ?>

        <?= Html::submitButton('Добавить автора') ?>
        <?= AutoComplete::widget([
            'id' => 'autocomplete-author',
            'clientOptions' => [
                'source' => $users,
                'select' => new JsExpression("function( event, ui ) {
                        $('#autocomplete-author').val(ui.item.label); 
                        $('#author-id').val(ui.item.value);
                        return false;
                 }"),
                'focus' => new JsExpression("function( event, ui ) {
                        $('#autocomplete-author').val(ui.item.label);
                        return false;                        
                 }"),

            ],

        ]); ?>
        <?= $form->field($author, 'user_id')->hiddenInput(['id' => 'author-id'])->label(''); ?>

        <?php ActiveForm::end(); ?>
        <ul>
            <h3>Авторы:</h3>
            <?php
            $isFirst = true;
            foreach ($model->authors as $author): ?>
                <li style="list-style: none;">
                    <div class="col-md-4">
                        <?= $author->profile->name ?>
                    </div>
                    <div class="col-md-8">
                        <?= !$isFirst ? '<a href="/admin/games/remove-author?user_id=' . $author->profile->user_id . '&game_id=' . $model->game_id . '"><div class="glyphicon glyphicon-trash"></div></a>' : '&nbsp;' ?>
                    </div>
                </li>

                <?php
                $isFirst = false;
            endforeach; ?>
        </ul>


    </div>
    <?php endif; ?>
</div>




<script>
    var start = $('#time-to-start').val();
    var stop = $('#time-to-stop').val();

    function getTimeRemaining(endtime) {
        var t = Date.parse(endtime) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };
    }

    function initializeClock(id, endtime, msg) {
        var clock = document.getElementById(id);
        var timeinterval = setInterval(function () {
            var t = getTimeRemaining(endtime);
            if (t.total <= 0) {
                clearInterval(timeinterval);
                window.location.reload();

            }

            clock.innerHTML = msg + ' <b>' + t.days + '</b> дней ' +
                '<b>' + t.hours + '</b> часов ' +
                '<b>' + t.minutes + '</b> минут ' +
                '<b>' + t.seconds + '</b> секунд';

        }, 1000);
    }

    var t_start = getTimeRemaining(start);
    if (t_start.total > 0) {
        initializeClock('countdown', start, "Игра начнётся через:");

    } else {
        var t_stop = getTimeRemaining(stop);
        if (t_stop.total > 0) {
            initializeClock('countdown', stop, "До конца игры осталось:");
        }
    }


</script>