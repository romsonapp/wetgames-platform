<?php

use yii\helpers\Html;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Games */

$this->title = 'Настройки игры: ' . $model->game_title;

?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4><?= Html::img($model->type->icon, ['width'=>50]) ?> <?= $model->type->label ?>: (#<?= $model->game_id ?>)
            "<?= $model->game_title ?>"</h4>
    </div>
    <div class="panel-body">
        <?= $this->render('game_menu', compact('model')) ?>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab">Основные настройки</a></li>
            <li role="presentation"><a href="#advanced" aria-controls="advanced" role="tab" data-toggle="tab">Дополнительные настройки</a></li>
            <li role="presentation"><a href="#authors" aria-controls="authors" role="tab" data-toggle="tab">Авторы</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="main">
                <br>
                <p><b>Взнос за участие:</b> <?= $model->fee ? $model->fee : 0 ?> UAH</p>
                <p><b>Начало:</b> <?= $model->game_start ?></p>
                <p><b>Конец:</b> <?= $model->game_stop ?></p>
                <p><b>Описание игры:</b></p>
                <p><?= $model->game_description ?></p>
                <?= Html::a('Редактировать', ['update', 'id' => $model->game_id]) ?>

            </div>
            <div role="tabpanel" class="tab-pane" id="advanced">
                <?php $form = ActiveForm::begin([
                    'action' => 'update?data-action=advanced&id=' . $model->game_id,
                    'method' => 'POST',
                ]); ?>
                <?= $form->field($model, 'online_statistic')->radioList([1 => 'Включить', 0 => 'Отключить']) ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="authors">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <br>
                <?php $form = ActiveForm::begin(); ?>

                <?= Html::submitButton('Добавить автора') ?>
                <?= AutoComplete::widget([
                    'id' => 'autocomplete-author',

                    'clientOptions' => [
                        'source' => $users,
                        'select' => new JsExpression("function( event, ui ) {
                        $('#autocomplete-author').val(ui.item.label); 
                        $('#author-id').val(ui.item.value);
                        return false;
                 }"),
                        'focus' => new JsExpression("function( event, ui ) {
                        $('#autocomplete-author').val(ui.item.label);
                        return false;                        
                 }"),
                    ],

                ]); ?>
                <?= $form->field($author, 'user_id')->hiddenInput(['id' => 'author-id'])->label(''); ?>

                <?php ActiveForm::end(); ?>
                    </div><div class="col-md-6">
                <ul>
                    <h3>Авторы:</h3>
                    <?php
                    $isFirst = true;
                    foreach ($model->authors as $author): ?>
                        <li style="list-style: none;">
                            <div class="col-md-4">
                                <?= $author->profile->name ?>
                            </div>
                            <div class="col-md-8">
                                <?= !$isFirst ? '<a href="/admin/games/remove-author?user_id=' . $author->profile->user_id . '&game_id=' . $model->game_id . '"><div class="glyphicon glyphicon-trash"></div></a>' : '&nbsp;' ?>
                            </div>
                        </li>

                        <?php
                        $isFirst = false;
                    endforeach; ?>
                </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php $this->registerJs("
 var url = document.location.toString();
 
    if (url.match('#')) {
    
        $('.nav-tabs a[href=\"#' + url.split('#')[1] + '\"]').tab('show');
    } //add a suffix

    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
    })
");