<?php
use yii\helpers\Html;

?>
<div class="cq-platform-admin-default-index">
    <h1>Админка</h1>

    <p><?= Html::a('Управление играми', Yii::$app->urlManager->createUrl(['admin/games'])); ?></p>
    <p><?= Html::a('Управление пользователями', Yii::$app->urlManager->createUrl(['admin/user'])); ?></p>
</div>
