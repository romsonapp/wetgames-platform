<?php
/* @var $game app\models\Games */
namespace app\widgets\Statistics;

use app\models\Games;
use app\models\Monitor;
use yii\base\Widget;

class MainStatWidget extends Widget
{
    const MAIN_STAT = 1;

    public $view;
    public $game;
    public $layout = 'statistic';

    public function run()
    {

        parent::run();

        switch ($this->view) {
            case self::MAIN_STAT:
                $this->view = 'main_stat';
                break;
            default:
                $this->view = 'main_stat';
                break;
        }

        $game = Games::find()->where(['game_id' => $this->game])->one();
        if(!$data = Monitor::getStatisticData($game->gameplay_id))
        {

        }
        if (!$game->isStatisticOpen() && !$game->isUserAuthor()) {
            $this->view = 'closed';
        }

        return $this->render($this->view, ['game' => $game, 'data' => $data]);
    }

}