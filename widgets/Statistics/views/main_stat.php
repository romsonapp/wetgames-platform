<div class="text-center">
    <h5>Статистика игры (#<?= $game->game_id ?>)
        "<?= \yii\helpers\Html::a($game->game_title, ['/game', 'id' => $game->game_id]) ?>"</h5>
</div>
<?php
if ($data == true):
    $index = key($data);
    $th_count = count($data[$index]['kills']);
    ?>

    <table class="stat-table">
        <thead>
        <th class="place"></th>
        <th>Охотник</th>
        <?php for ($i = 1; $i <= $th_count; $i++): ?>
            <th>Жертва #<?= $i ?></th>
        <?php endfor; ?>
        <th>Кем убит</th>
        <th class="place"></th>
        </thead>
        <tbody>
        <?php $place = 1;
        foreach ($data as $hunter):
            ?>

            <tr>
                <td class="place"><?= $place ?></td>
                <td class="hunter">
                    <div class="stat-badge"><?= count($hunter['kills']) ?></div>
                    <?= $hunter['print'] ?>
                    <br>
                    (<?= $hunter['how_long'] ?>)
                </td>
                <?php $count = $th_count;
                foreach ($hunter['kills'] as $victim): ?>
                    <td class="victim">
                        <?= $victim['print']['name'] ?>
                        <br>
                        <?= $victim['print']['date'] ?>
                        <br>
                        <?= $victim['print']['time'] . $victim['print']['time_micro'] ?>
                        <br>
                        (<?= $victim['print']['time_after_start'] ?>)
                    </td>

                    <?php $count--; endforeach; ?>
                <?php if ($count < $th_count): ?>
                    <?php for ($c = 0; $c < $count; $c++): ?>
                        <td class="victim"></td>
                    <?php endfor; ?>

                <?php endif; ?>
                <td class="killer">
                    <?php if (isset($hunter['death'])): ?>
                        <?= $hunter['death']['print']['name'] ?>
                        <br>
                        <?= $hunter['death']['print']['date'] ?>
                        <br>
                        <?= $hunter['death']['print']['time'] . $hunter['death']['print']['time_micro'] ?>
                    <?php endif; ?>
                </td>
                <td class="place"><?= $place ?></td>
            </tr>
            <?php $place++; endforeach; ?>
        </tbody>
        <tfoot>
        <th class="place"></th>
        <th>Охотник</th>
        <?php for ($i = 1; $i <= $th_count; $i++): ?>
            <th>Жертва #<?= $i ?></th>
        <?php endfor; ?>
        <th>Кем убит</th>
        <th class="place"></th>
        </tfoot>
    </table>

<?php else: ?>
    Нет данных.
<?php endif; ?>
