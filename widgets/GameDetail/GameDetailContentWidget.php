<?php

namespace app\widgets\GameDetail;


use app\models\Gameplay;
use yii\base\Widget;
use yii\helpers\Html;

class GameDetailContentWidget extends Widget
{
    const GAME_DETAIL = 1;
    const MAIN_PAGE = 2;
    public $game;
    public $view;
    public $p_played_count;
    public $p_invited_count;
    public $type = self::GAME_DETAIL;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        switch ($this->type) {
            case self::GAME_DETAIL:
                $this->view = 'game_detail';
                break;
            case  self::MAIN_PAGE:
                $this->view = 'main_page';
                break;

        }
        $p_invited_count = count(Gameplay::getInvitedPlayers($this->game->gameplay_id));
        $p_played_count = count(Gameplay::getPlayedPlayers($this->game->gameplay_id));
        return $this->render('game_detail', ['game' => $this->game, 'p_played_count' => $p_played_count, 'p_invited_count' => $p_invited_count, 'type' => $this->type]);
    }
}