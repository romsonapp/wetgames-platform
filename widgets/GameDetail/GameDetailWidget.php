<?php

namespace app\widgets\GameDetail;


use app\models\Gameplay;
use yii\base\Widget;
use yii\helpers\Html;

class GameDetailWidget extends Widget
{
    const GAME_DETAIL = 1;
    const MAIN_PAGE = 2;
    public $game;
    public $view;

    public $type = self::GAME_DETAIL;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
       /* switch ($this->type) {
            case self::GAME_DETAIL:
                $this->view = 'game_detail';
                break;
            case  self::MAIN_PAGE:
                $this->view = 'main_page';
                break;

        }*/

        return $this->render('game_body', ['type' => $this->type, 'game' => $this->game]);
    }
}