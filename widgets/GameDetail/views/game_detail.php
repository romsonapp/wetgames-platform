<?php
use app\models\Gameplay;
use app\models\GamesAuthors;
use yii\helpers\Html;

/* @var $game app\models\Games */

?>
<p><?= Html::label("Автор игры:") ?> <?= GamesAuthors::getAuthorsToString($game) ?></p>
<hr>
<p><?= Html::label("Тип игры:") ?> <?= $game->type->label ?></p>
<p><?= Html::label("Начало игры:") ?> <?= $game->game_start ?></p>
<p><?= Html::label("Время окончания:") ?> <?= $game->game_stop ?></p>
<p><?= Html::label("Взнос за участие:") ?> <?= $game->fee ?> UAH
    <?php if (Yii::$app->user->identity): ?>
    <?php if (Yii::$app->user->identity->checkProfile()): ?>
        <?php if (!$game->checkInvite() && !$game->isStarted()): ?>
            <?= Html::a('подать заявку на участие', ['/game/invite', 'id' => $game->game_id], ['data' => ['method' => 'post']]); ?>
        <?php endif; ?>

    <?php else: ?>
        <br>
        <u><?= Html::encode("Для подачи заявки на игру, нужно заполнить все поля в досье!"); ?></u>
    <?php endif; ?>




    <?php if ($game->canPlayerEntry()): ?>
        <a class="btn btn-default" href="/go?game_id=<?= $game->game_id ?>">Войти в
            игру</a>
    <?php endif; ?>
    <?php if ($game->checkInvite() && !$game->isStarted()):
    $invite_text = $game->isInvited() ? 'Вы приняты в игру' : "Вы подали заявку, на участие в игре";
    ?>
    <p><u><?= Html::tag(false, $invite_text) ?></u>
        (<?= Html::a('отменить заявку', ['/game/cancel-invite', 'id' => $game->game_id], ['data' => ['method' => 'post']]); ?>
        )</p></p>
<?php endif; ?>
<?php endif; ?>
<hr>
<?php if ($type == \app\widgets\GameDetail\GameDetailWidget::GAME_DETAIL && $game->isClosed()): ?>
    <p><?= Html::label("В игре приняли участие: " . $p_played_count . " участников") ?>
        (<?= Html::a('статистика', ['/statistic', 'game' => $game->game_id]) ?>)</p>
    Статус игры: Игра недоступна для прохождения
    <hr>
<?php endif; ?>
<p><?= Html::label("Подтвердили участие " . $p_invited_count . " участников") ?></p>
<p><?= Html::label("Приняты  к участию " . $p_played_count . " участников") ?></p>
<hr>
<?php if ($type == \app\widgets\GameDetail\GameDetailWidget::GAME_DETAIL): ?>
    <div class="game-description">
        <h4>Описание игры:</h4>
        <?= $game->game_description ?>
    </div>
<?php endif; ?>
