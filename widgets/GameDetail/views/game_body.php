<?php

/* @var $this app\widgets\GameDetail\GameDetailWidget */
use app\models\Gameplay;
use app\models\GamesAuthors;
use app\widgets\GameDetail\GameDetailContentWidget;
use app\widgets\GameDetail\GameDetailWidget;
use yii\helpers\Html;

/* @var $game app\models\Games */

?>

<div class="panel panel-info">
    <div class="panel-heading">
        <h4><?= Html::img($game->type->icon, ['width'=>50]) ?> <?= $game->type->label ?>: (#<?= $game->game_id ?>)
            "<?= $game->game_title ?>"</h4>
    </div>
    <div class="panel-body">
        <?php if($type == GameDetailWidget::MAIN_PAGE): ?>
        <?= GameDetailContentWidget::widget(['game' => $game, 'type' => GameDetailWidget::MAIN_PAGE ]); ?>
        <?php endif; ?>
        <?php if($type == GameDetailWidget::GAME_DETAIL): ?>
            <?= GameDetailContentWidget::widget(['game' => $game, 'type' => GameDetailWidget::GAME_DETAIL ]); ?>
        <?php endif; ?>
    </div>
    <?php if($type == GameDetailWidget::MAIN_PAGE): ?>
    <div class="panel-footer text-center" style="padding-bottom: 1px;">
        <p><?= Html::a('Детали игры', ['/game', 'id'=>$game->game_id]) ?></p>
    </div>
    <?php endif; ?>
</div>

