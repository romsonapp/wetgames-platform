<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'admin'],
    'aliases' => [
        '@admin' => '@app/modules/admin',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'xdcfgjmk',
        ],

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/views/user',
                    '@dektrium/user/views/admin' => '@admin/modules/user',
                    '@dektrium/rbac/views' => '@admin/modules/rbac',

                ],
            ],
        ],

        /*'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp-relay.gmail.com',
                'username' => 'romsonapp@gmail.com',
                'password' => 'Dkflbckfd88',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],*/
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'wet_wars_games' => require(__DIR__ . '/db_games.php'),
        'wet_wars_monitor' => require(__DIR__ . '/db_monitor.php'),

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'archive' => 'site/archive'
            ],
        ],

    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],

        'rbac' => [
            'class' => 'dektrium\rbac\RbacWebModule',
            'viewPath' => '@admin/modules/rbac/views',
        ],
        'user' => [
            //'class' => 'dektrium\user\Module',
            'class' => 'app\modules\admin\modules\user\DektriumUser',
            'enableUnconfirmedLogin' => true,
            //'viewPath' => '@app/views/user',
            'enableConfirmation' => false,

            'modelMap' => [
                'Profile' => 'app\models\Profile',
                'RegistrationForm' => 'app\models\user\RegistrationForm',
                'User' => 'app\models\User',
            ],
            'controllerMap' => [
                'settings' => 'app\controllers\user\SettingsController',
                'registration' => 'app\controllers\user\RegistrationController',
                'security' => 'app\controllers\user\SecurityController',
                'profile' => 'app\controllers\user\ProfileController',
                'admin' => 'app\modules\admin\controllers\user\AdminController',
            ],

            'cost' => 12,
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
