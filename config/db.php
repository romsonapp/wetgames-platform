<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=wet_wars_platform',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
