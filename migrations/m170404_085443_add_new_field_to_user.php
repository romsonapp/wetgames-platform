<?php

use yii\db\Migration;

class m170404_085443_add_new_field_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'login', \yii\db\Schema::TYPE_STRING);
        $this->addColumn('{{%user}}', 'phone', \yii\db\Schema::TYPE_STRING);
    }

    public function down()
    {
        echo "m170404_085443_add_new_field_to_user cannot be reverted.\n";
        $this->dropColumn('{{%user}}', 'login');
        $this->dropColumn('{{%user}}', 'phone');
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
