<?php

use yii\db\Migration;
use yii\db\Schema;

class m170404_100958_add_new_field_to_profile extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profile}}', 'middlename', Schema::TYPE_STRING);
        $this->addColumn('{{%profile}}', 'lastname', Schema::TYPE_STRING);
        $this->addColumn('{{%profile}}', 'gender', Schema::TYPE_STRING);
        $this->addColumn('{{%profile}}', 'birthday_date', Schema::TYPE_STRING);
        $this->addColumn('{{%profile}}', 'birthday_year', Schema::TYPE_INTEGER);

        $this->addColumn('{{%profile}}', 'city', Schema::TYPE_STRING);
        $this->addColumn('{{%profile}}', 'work_address', Schema::TYPE_TEXT);
        $this->addColumn('{{%profile}}', 'study_address', Schema::TYPE_TEXT);
        $this->addColumn('{{%profile}}', 'address', Schema::TYPE_TEXT);
        $this->addColumn('{{%profile}}', 'schedule', Schema::TYPE_TEXT);

        $this->addColumn('{{%profile}}', 'transport_mark', Schema::TYPE_STRING);
        $this->addColumn('{{%profile}}', 'transport_model', Schema::TYPE_STRING);
        $this->addColumn('{{%profile}}', 'transport_name', Schema::TYPE_STRING);




    }

    public function down()
    {
        echo "m170404_100958_add_new_field_to_profile cannot be reverted.\n";
        $this->dropColumn('{{%profile}}', 'middlename');
        $this->dropColumn('{{%profile}}', 'lastname');
        $this->dropColumn('{{%profile}}', 'gender');
        $this->dropColumn('{{%profile}}', 'birthday_date');
        $this->dropColumn('{{%profile}}', 'birthday_year');

        $this->dropColumn('{{%profile}}', 'city');
        $this->dropColumn('{{%profile}}', 'work_address');
        $this->dropColumn('{{%profile}}', 'study_address');
        $this->dropColumn('{{%profile}}', 'address');
        $this->dropColumn('{{%profile}}', 'schedule');

        $this->dropColumn('{{%profile}}', 'transport_mark');
        $this->dropColumn('{{%profile}}', 'transport_model');
        $this->dropColumn('{{%profile}}', 'transport_name');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
