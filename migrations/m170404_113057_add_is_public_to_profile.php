<?php

use yii\db\Migration;

class m170404_113057_add_is_public_to_profile extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profile}}', 'is_public_contacts', \yii\db\Schema::TYPE_INTEGER."(11) DEFAULT 0");
    }

    public function down()
    {
        echo "m170404_113057_add_is_public_to_profile cannot be reverted.\n";
        $this->dropColumn('{{%profile}}', 'is_public_contacts');
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
