-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Апр 05 2017 г., 04:11
-- Версия сервера: 5.7.17-0ubuntu0.16.04.1
-- Версия PHP: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `wet_wars_platform`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('author', '12', 1491073224),
('owner', '11', 1490976257),
('owner', '16', 1491073597);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('author', 1, 'author', 'game', NULL, 1490976071, 1490976071),
('owner', 1, 'owner', 'game', NULL, 1490975794, 1490975794);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_rule`
--

INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('game', 0x4f3a32343a226170705c6d616e61676572735c52756c654d616e61676572223a333a7b733a343a226e616d65223b733a343a2267616d65223b733a393a22637265617465644174223b4e3b733a393a22757064617465644174223b693a313439303937363837343b7d, 1490973417, 1490976874);

-- --------------------------------------------------------

--
-- Структура таблицы `games`
--

CREATE TABLE `games` (
  `game_id` int(11) NOT NULL,
  `game_title` varchar(255) NOT NULL,
  `game_description` text NOT NULL,
  `game_start` int(11) NOT NULL,
  `game_stop` int(11) NOT NULL,
  `gameplay_id` varchar(255) DEFAULT NULL,
  `fee` int(11) DEFAULT '0',
  `invites_available` int(11) DEFAULT '1',
  `online_statistic` int(11) DEFAULT '0',
  `status_id` int(11) NOT NULL DEFAULT '1',
  `type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `games`
--

INSERT INTO `games` (`game_id`, `game_title`, `game_description`, `game_start`, `game_stop`, `gameplay_id`, `fee`, `invites_available`, `online_statistic`, `status_id`, `type_id`) VALUES
(70, 'Последняя.Контрольная. В голову!', '<p><strong><img alt="" src="http://d1.endata.cx/data/games/53784/%d0%bc%d0%be%d0%ba%d1%80%d1%83%d1%85%d0%b0.jpg" style="height:510px; margin-left:auto; margin-right:auto; width:600px" /></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Суть игры:&nbsp;</strong><br />\r\n&nbsp;</p>\r\n\r\n<p><strong>Суть игры заключается в том, что каждый участник является одновременно и &laquo;охотником&raquo; и &laquo;жертвой&raquo;. Участник получает на сайте досье на &laquo;жертву&raquo; и начинает &laquo;охоту&raquo; на неё, в это же время досье этого участника получает другой игрок. Цель &laquo;охотника&raquo; &mdash; выследить &laquo;жертву&raquo; и в прямом смысле замочить её &mdash; из водяного оружия. Цель &laquo;жертвы&raquo; &mdash; не попасться в лапы &laquo;охотника&raquo; и не дать себя &laquo;замочить&raquo;. Если участник &laquo;замочил&raquo; свою &laquo;жертву&raquo;, она отдаёт ему свой код жизни, который необходимо ввести в форму на сайте. Так участник получает на сайте новое досье &mdash; на человека, за которым охотилась &laquo;замоченная жертва&raquo;. Таким образом, круг участников сужается и в финале остаётся только два &laquo;охотника&raquo;, которые выслеживают друг друга.&nbsp;</strong><br />\r\n<br />\r\n<strong>Ваша миссия, если вы за нее возьметесь, состоит в том, чтобы &mdash; &laquo;замочить&raquo; максимальное количество &laquo;жертв&raquo; из водяного оружия и остаться &laquo;незамоченным&raquo;.&nbsp;</strong><br />\r\n<strong>&laquo;Мочить&raquo; жертву можно только из водяного оружия (пистолеты, ружья, автоматы) и только чистой водой.&nbsp;</strong><br />\r\n<br />\r\n<strong>Для участия в игре необходимо:&nbsp;</strong></p>\r\n\r\n<p><strong>Полностью заполнить свой профиль (номер телефона, адрес проживания, адрес работы/учебы). Так же по вашему желанию вы можете выбрать так называемую &quot;мирную зону&quot; - зона где на вас не могут охотиться, работа, машина, квартира, что угодно. Об этом следует написать в графе &quot;Распорядок дня&quot;. Также и вы НЕ МОЖЕТЕ охотиться из своей мирной зоны;</strong></p>\r\n\r\n<p><strong>Компьютер или телефон с подключением к интернету для получения досье на свою &quot;жертву&quot;;</strong></p>\r\n\r\n<p><strong>Любой водный пистолет или водяные бомбы (Читаем правила);</strong></p>\r\n\r\n<p><u><strong>Телефон организатора:</strong></u></p>\r\n\r\n<p><strong>098-123-45-67</strong></p>\r\n\r\n<p><u><strong>Требования:&nbsp;</strong></u></p>\r\n\r\n<p><strong>Полностью заполненное личное дело:</strong></p>\r\n\r\n<p><strong>Фотографии рост и портрет, должны соответствовать тому, как вы выглядите сегодня;</strong></p>\r\n\r\n<p><strong>Распорядок дня, как можно более полный и подробный;</strong></p>\r\n\r\n<p><strong>Мирная зона, должна быть четко аргументирована. &quot;Мирная зона&quot; - зона где на вас не могут охотиться, а также и вы не можете;</strong></p>\r\n\r\n<p><strong>Каждый профиль будет проверяться организаторами.</strong></p>\r\n\r\n<p><strong>Участники, неточно, неверно, или не полностью заполнившие данные, будут не допущены к игре.</strong></p>\r\n\r\n<p><strong>А если такие моменты выяснятся в процессе игры, то дисквалифицированы.</strong></p>\r\n', 1491333480, 1492027200, 'gameplay_70', NULL, 1, 0, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `games_authors`
--

CREATE TABLE `games_authors` (
  `id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `games_authors`
--

INSERT INTO `games_authors` (`id`, `game_id`, `user_id`) VALUES
(1, 32, 11),
(2, 33, 12),
(3, 34, 12),
(10, 32, 13),
(11, 34, 11),
(13, 35, 11),
(14, 35, 12),
(15, 36, 11),
(16, 38, 11),
(17, 56, 11),
(18, 58, 11),
(19, 59, 11),
(20, 60, 11),
(22, 60, 13),
(23, 61, 13),
(24, 62, 12),
(26, 63, 11),
(27, 62, 11),
(28, 64, 11),
(29, 65, 11),
(30, 66, 11),
(31, 67, 11),
(32, 68, 11),
(33, 69, 12),
(34, 70, 11),
(35, 71, 11),
(44, 70, 13);

-- --------------------------------------------------------

--
-- Структура таблицы `game_status`
--

CREATE TABLE `game_status` (
  `id` int(11) NOT NULL,
  `label` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `game_status`
--

INSERT INTO `game_status` (`id`, `label`) VALUES
(1, 'Открытая'),
(2, 'Закрытая');

-- --------------------------------------------------------

--
-- Структура таблицы `game_type`
--

CREATE TABLE `game_type` (
  `id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `icon` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `game_type`
--

INSERT INTO `game_type` (`id`, `label`, `icon`) VALUES
(1, 'Мокрые войны', '/uploads/icons/type.3.png');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1490878635),
('m140209_132017_init', 1490878646),
('m140403_174025_create_account_table', 1490878648),
('m140504_113157_update_tables', 1490878654),
('m140504_130429_create_token_table', 1490878656),
('m140506_102106_rbac_init', 1490972474),
('m140830_171933_fix_ip_field', 1490878657),
('m140830_172703_change_account_table_name', 1490878658),
('m141222_110026_update_ip_field', 1490878658),
('m141222_135246_alter_username_length', 1490878659),
('m150614_103145_update_social_account_table', 1490878664),
('m150623_212711_fix_username_notnull', 1490878664),
('m151218_234654_add_timezone_to_profile', 1490878665),
('m160929_103127_add_last_login_at_to_user_table', 1490878666),
('m170404_085443_add_new_field_to_user', 1491296806),
('m170404_100958_add_new_field_to_profile', 1491301103),
('m170404_113057_add_is_public_to_profile', 1491305600);

-- --------------------------------------------------------

--
-- Структура таблицы `player_status`
--

CREATE TABLE `player_status` (
  `status_id` int(11) NOT NULL,
  `status` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `player_status`
--

INSERT INTO `player_status` (`status_id`, `status`) VALUES
(1, 'live'),
(2, 'death'),
(3, 'invited_in_game');

-- --------------------------------------------------------

--
-- Структура таблицы `profile`
--

CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` text COLLATE utf8_unicode_ci,
  `middlename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday_year` int(11) DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_address` text COLLATE utf8_unicode_ci,
  `study_address` text COLLATE utf8_unicode_ci,
  `address` text COLLATE utf8_unicode_ci,
  `schedule` text COLLATE utf8_unicode_ci,
  `transport_mark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transport_model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transport_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_public_contacts` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`, `timezone`, `photo`, `middlename`, `lastname`, `gender`, `birthday_date`, `birthday_year`, `city`, `work_address`, `study_address`, `address`, `schedule`, `transport_mark`, `transport_model`, `transport_name`, `is_public_contacts`) VALUES
(11, 'Владислав', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', 'моё досье', 'Europe/Kiev', 'uploads/photo_user_11.jpg', 'Олегович', 'Черепеня', 'Мужчина', '11 мая', 1988, 'Кропивницкий', 'Дома', '', 'ул.Вокзальная 20, к.2 кв.61', 'Работаю дома, иногда с 12:00 до 14:00 выхожу по делам в центр. Обязательно захожу в "Мокко".', 'ВАЗ', '21053', '', 0),
(12, 'USER1', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', '', NULL, 'uploads/photo_user_12.jpg', 'USER1', 'USER1', 'Женщина', '', NULL, 'qweqwe', 'qweqwew', 'qwewqe', 'qweqwe', 'qwewqeqwe', '', '', '', 0),
(13, 'Роман', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', '', NULL, 'uploads/photo_user_13.jpg', 'Черепеня', 'Владиславович', 'Мужчина', '22 августа', 2013, 'Кировоград мой дом', 'Гуляю в игрушки', 'учЮ букви', 'С рАдитИлями', 'УтрАм гуляйу и смАтрю телИвизор. ПАтом прАшу маму пАйти гулять вА двор. Но мама всйо время гАвАрит Што пАтом пАйдём. Ну ничего, я всё равно её люблю.', 'Мотык', 'Крутой', '', 0),
(14, 'Григорий', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', 'Живу там-то... Гуляю там-то... во столько-то....', 'Pacific/Apia', 'uploads/photo_user_14.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(34, 'Галина', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'uploads/photo_user_34.jpg', 'Юрьевна', 'Черепеня', 'Женщина', '27 января ', 1991, 'Кропивницкий ', 'Даддвжвв', 'Осел ', 'Дома 7', 'Ладвэриль', '', '', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `social_account`
--

CREATE TABLE `social_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `token`
--

CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `last_login_at`, `login`, `phone`) VALUES
(11, 'admin', 'romsonapp@gmail.com', '$2y$12$oA3F.Y5eWAexG9aNSdZJKOh7aZLSEMVy1GfDmMs/s/Udsbuo.RHFa', 'nNkpbSBsTKlhZJ467tj5lXfRkutcYb0m', 1490879953, NULL, NULL, '127.0.0.1', 1490879953, 1490879953, 0, 1491343437, NULL, '+380955696565'),
(12, 'user1', 'hello@r.com', '$2y$12$KNAg88/j25V7A7QNe2Hsnu6JzbUO9TSqtqJNm8XzKuc9pnfKiOqVC', 'fGcHZBASsnwM96Zlh5fy2xOlEb5QtcdB', 1490908945, NULL, NULL, '127.0.0.1', 1490908945, 1490908945, 0, 1491334477, NULL, NULL),
(13, 'user2', 'user2@g.com', '$2y$12$sVOkAPYZNqvDTy3hHZ01D.2zsdpu8U4rBuHyTTeZjkvYM9n98OShC', 'aal1ULxjZERiP9GmqdtodIxaMuJAmN8M', 1490908960, NULL, NULL, '127.0.0.1', 1490908960, 1490908960, 0, 1491350396, NULL, NULL),
(14, 'user3', 'user3@g.com', '$2y$12$JC55n5zqcKCAdH7ZzxgJ.OWGA3fn.PzREoWL8F8yFsVy7tjvLBSIW', 'ibFumjSNGqrKogP3ObMJ-ROQEKH_kE5o', 1490908972, NULL, NULL, '127.0.0.1', 1490908972, 1490908972, 0, 1491141691, NULL, NULL),
(16, 'Den1ska', 'den1ska@ukr.net', '$2y$12$Kn2g7jtkKB5TLcjmwoEeSOCUfeJy3NeUHjedtDiHUMZDsEqCHoaEG', 'hZasb9VMZSYt0dMtuYTKLlsG71ELOqvu', 1491073531, NULL, NULL, '127.0.0.1', 1491073531, 1491073531, 0, 1491299913, NULL, NULL),
(18, 'Si7Ro', 'web@romsonapp.com', '$2y$12$vdBZvEsR0i7ln0vwvnsoQuX/ohvzMLarhGafOldMRpkJ4ASZM08gm', 'Dxz67aLuD5-hNcBN_t9OQKoNWK6tjThd', 1491297505, NULL, NULL, '127.0.0.1', 1491297505, 1491297505, 0, NULL, NULL, '+380955696565'),
(34, 'lovelife', 'lovelife@gmail.com', '$2y$12$Y19x6LfgwRF/4StLGkaTv.86Wdy8yg68XY8Ryg0i3/rIeP/WZ7Y.i', 'n2MqR1GoOadA-aPjtM-yBvSttHbD-_c4', 1491302425, NULL, NULL, '127.0.0.1', 1491302425, 1491302425, 0, 1491350489, NULL, '+380665576808');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`game_id`);

--
-- Индексы таблицы `games_authors`
--
ALTER TABLE `games_authors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `game_status`
--
ALTER TABLE `game_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `game_type`
--
ALTER TABLE `game_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `player_status`
--
ALTER TABLE `player_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Индексы таблицы `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_unique` (`provider`,`client_id`),
  ADD UNIQUE KEY `account_unique_code` (`code`),
  ADD KEY `fk_user_account` (`user_id`);

--
-- Индексы таблицы `token`
--
ALTER TABLE `token`
  ADD UNIQUE KEY `token_unique` (`user_id`,`code`,`type`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_username` (`username`),
  ADD UNIQUE KEY `user_unique_email` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `games`
--
ALTER TABLE `games`
  MODIFY `game_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT для таблицы `games_authors`
--
ALTER TABLE `games_authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT для таблицы `game_status`
--
ALTER TABLE `game_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `game_type`
--
ALTER TABLE `game_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `player_status`
--
ALTER TABLE `player_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `social_account`
--
ALTER TABLE `social_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
