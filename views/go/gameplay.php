<?php use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="row">


    <?php $form = ActiveForm::begin([
        'id' => 'profile-form',
        'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-lg-9\">{error}\n{hint}</div>",
            'labelOptions' => ['class' => 'col-lg-3 control-label'],
        ],
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'validateOnBlur' => false,
    ]); ?>


    <div class="form-group">
        <div class="col-sm-12">
            <br>
            <?= Html::label('Введите код жизни жертвы:', 'lifecode') ?>
            <?= Html::input('text','lifecode','',['class'=>'form-control'])?>
            <br>


            <?= Html::submitButton(Yii::t('user', 'Убить'), ['class' => 'btn btn-block btn-success']) ?>
            <?php if (Yii::$app->session->hasFlash('error')): ?>
                <br>
                <div class="alert alert-danger alert-dismissable">
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
            <?php endif; ?>
            <br>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

    <h3>Ваш код жизни: <span style="color: red;"><?= $model->getFormattedLifecode() ?></span></h3>
    <h1>Досье жертвы</h1>
    <hr>
    <h3>Личные данные</h3>

    <?php if (!empty($profile->profile->name)): ?>
        <b><?= Html::encode('Имя: ') ?></b><?= Html::encode($profile->profile->name) ?><br>
    <?php endif; ?>
    <?php if (!empty($profile->profile->middlename)): ?>
        <b><?= Html::encode('Отчество: ') ?></b><?= Html::encode($profile->profile->middlename) ?><br>
    <?php endif; ?>
    <?php if (!empty($profile->profile->lastname)): ?>
        <b><?= Html::encode('Фамилия: ') ?></b><?= Html::encode($profile->profile->lastname) ?><br>
    <?php endif; ?>
    <?php if (!empty($profile->profile->gender)): ?>
        <b><?= Html::encode('Пол: ') ?></b><?= Html::encode($profile->profile->gender) ?><br>
    <?php endif; ?>
    <?php if (!empty($profile->profile->birthday_date)): ?>
        <b><?= Html::encode('Дата рождения: ') ?></b><?= Html::encode($profile->profile->birthday_date) ?><br>
    <?php endif; ?>

    <?php if (!empty($profile->profile->birthday_year)): ?>
        <b><?= Html::encode('Год рождения: ') ?></b><?= Html::encode($profile->profile->birthday_year) ?><br>
    <?php endif; ?>
    <hr>
    <h3>Контактные данные</h3>
    <?php if (!empty($profile->phone)): ?>
        <b><?= Html::encode('Мобильный телефон: ') ?></b><?= Html::encode($profile->phone) ?><br>
    <?php endif; ?>
    <hr>

    <h3>Места обитания</h3>
    <br>
    <?php if (!empty($profile->profile->work_address)): ?>
        <b><?= Html::encode('Место и адрес работы:') ?></b>
        <br><?= Html::encode($profile->profile->work_address) ?><br>
    <?php endif; ?>
    <?php if (!empty($profile->profile->study_address)): ?>
        <b><?= Html::encode('Место и адрес учебы:') ?></b>
        <br><?= Html::encode($profile->profile->study_address) ?><br>
    <?php endif; ?>
    <?php if (!empty($profile->profile->address)): ?>
        <b><?= Html::encode('Адрес проживания:') ?></b><br><?= Html::encode($profile->profile->address) ?>
        <br>
    <?php endif; ?>
    <?php if (!empty($profile->profile->schedule)): ?>
        <b><?= Html::encode('Приблизительный распорядок дня:') ?></b>
        <br><?= Html::encode($profile->profile->schedule) ?><br>
    <?php endif; ?>
    <hr>
    <?php if (!empty($profile->profile->transport_mark) || !empty($profile->profile->transport_model) || !empty($profile->profile->transport_name)): ?>
        <h3>Транспорт</h3>
        <?php if (!empty($profile->profile->transport_mark)): ?>
            <b><?= Html::encode('Марка:') ?></b><br><?= Html::encode($profile->profile->transport_mark) ?>
            <br>
        <?php endif; ?>
        <?php if (!empty($profile->profile->transport_model)): ?>
            <b><?= Html::encode('Модель:') ?></b><br><?= Html::encode($profile->profile->transport_model) ?>
            <br>
        <?php endif; ?>
        <?php if (!empty($profile->profile->transport_name)): ?>
            <b><?= Html::encode('Гос. номер, имя:') ?></b>
            <br><?= Html::encode($profile->profile->transport_name) ?><br>
        <?php endif; ?>
        <hr>
    <?php endif; ?>

    <div>
    <?= Html::a(Html::img($profile->profile->getAvatarUrl(), [
        'class' => 'img-rounded img-responsive',
        'width' => 200,
        'style' => 'margin-bottom: 20px;'
    ]), [$profile->profile->getAvatarUrl()]) ?>
    </div>
</div>
