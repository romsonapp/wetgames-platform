<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use dektrium\user\helpers\Timezone;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $model
 */

$this->title = Yii::t('user', 'Ваш профайл');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="row">
    <div class="col-md-3">
        <?= $this->render('_menu') ?>
    </div>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'profile-form',
                    'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-lg-9\">{error}\n{hint}</div>",
                        'labelOptions' => ['class' => 'col-lg-3 control-label'],
                    ],
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'validateOnBlur' => false,
                ]); ?>
                <div class="text-center"><u><h3>Личные даные</h3></u></div>
                <?= $form->field($model, 'name')->label('Имя') ?>
                <?= $form->field($model, 'lastname')->label('Фамилия') ?>
                <?= $form->field($model, 'middlename')->label('Отчество') ?>
                <?= $form->field($model, 'gender')->label('Пол')->radioList(['Мужчина' => 'Мужчина', 'Женщина' => 'Женщина']) ?>
                <?= $form->field($model, 'birthday_date')->label('День и месяц рождения') ?>
                <?= $form->field($model, 'birthday_year')->label('Год рождения') ?>

                <?= $form->field($model, 'is_public_contacts')->label('Скрыть контактные данные (телефон, email)?')->radioList([0 => "Да", 1 => "Нет"]) ?>
                <div class="text-center"><u><h3>Места обитания</h3></u></div>

                <?= $form->field($model, 'city')->label("Город") ?>
                <?= $form->field($model, 'work_address')->label("Работа")->textarea()->hint('Место и адрес работы') ?>
                <?= $form->field($model, 'study_address')->label("Учеба")->textarea()->hint('Место и адрес учебы') ?>
                <?= $form->field($model, 'address')->label("Адрес проживания")->textarea() ?>
                <?= $form->field($model, 'schedule')->label("Досье")->textarea(['rows'=>10])->hint("Приблизительный распорядок дня") ?>

                <div class="text-center"><u><h3>Транспорт</h3></u></div>
                <?= $form->field($model, 'transport_mark')->label("Марка") ?>
                <?= $form->field($model, 'transport_model')->label("Модель") ?>
                <?= $form->field($model, 'transport_name')->label("Гос. номер, имя") ?>
                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-success']) ?>
                        <br>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
