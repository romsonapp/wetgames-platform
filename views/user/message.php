<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var yii\web\View $this
 * @var dektrium\user\Module $module
 */

use yii\bootstrap\Alert;
Yii::$app->view->registerMetaTag(["3;url=/"], 'refresh');
$this->title = $title;
?>


<div class="row">
    <div class="col-xs-12">
        <?= Alert::widget([
            'options' => ['class' => 'alert-dismissible alert-success'],
            'body' => 'Ваш аккаунт успешно зарегистрирован. Теперь вы можете авторизоваться.<br>Вы будете переадресованны на главную страницу через 3 сек. Если переадресация непроизошла, перейдите по <a href="/">этой ссылке</a>'
        ]) ?>
    </div>
</div>
