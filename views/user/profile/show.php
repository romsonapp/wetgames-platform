<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var \dektrium\user\models\Profile $profile
 */

$this->title = empty($profile->name) ? Html::encode($profile->user->username) : Html::encode($profile->name);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-12">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <?= Html::img($profile->getAvatarUrl(230), [
                    'class' => 'img-rounded img-responsive',
                    'alt' => $profile->user->username,
                ]) ?>
            </div>
            <div class="col-sm-6 col-md-8">

                <h4><?= $profile->user->username ?></h4>

                <ul style="padding: 0; list-style: none outside none;">
                    <?php if (!empty($profile->city)): ?>
                        <li>
                            <i class="glyphicon glyphicon-map-marker text-muted"></i> <?= Html::encode($profile->city) ?>
                        </li>
                    <?php endif; ?>

                    <?php if (!empty($profile->public_email)): ?>
                        <li>
                            <i class="glyphicon glyphicon-envelope text-muted"></i> <?= Html::a(Html::encode($profile->public_email), 'mailto:' . Html::encode($profile->public_email)) ?>
                        </li>
                    <?php endif; ?>
                    <li>
                        <i class="glyphicon glyphicon-time text-muted"></i> <?= Yii::t('user', 'В проекте с {0, date}', $profile->user->created_at) ?>
                    </li>
                </ul>
                <?php if (Yii::$app->user->identity && Yii::$app->user->identity->getId() == $profile->user_id): ?>
                    <span>(<?= Html::a('редактировать профиль', ['/user/settings']) ?>)</span>
                <?php endif; ?>
                <hr>
                <h3>Личные данные</h3>
                <?php if (!empty($profile->name)): ?>
                    <b><?= Html::encode('Имя: ') ?></b><?= Html::encode($profile->name) ?><br>
                <?php endif; ?>
                <?php if (!empty($profile->middlename)): ?>
                    <b><?= Html::encode('Отчество: ') ?></b><?= Html::encode($profile->middlename) ?><br>
                <?php endif; ?>
                <?php if (!empty($profile->lastname)): ?>
                    <b><?= Html::encode('Фамилия: ') ?></b><?= Html::encode($profile->lastname) ?><br>
                <?php endif; ?>
                <?php if (!empty($profile->gender)): ?>
                    <b><?= Html::encode('Пол: ') ?></b><?= Html::encode($profile->gender) ?><br>
                <?php endif; ?>
                <?php if (!empty($profile->birthday_date)): ?>
                    <b><?= Html::encode('Дата рождения: ') ?></b><?= Html::encode($profile->birthday_date) ?><br>
                <?php endif; ?>

                <?php if (!empty($profile->birthday_year)): ?>
                    <b><?= Html::encode('Год рождения: ') ?></b><?= Html::encode($profile->birthday_year) ?><br>
                <?php endif; ?>
                <hr>

                <?php if (Yii::$app->user->identity && Yii::$app->user->identity->getId() == $profile->user_id || $profile->is_public_contacts): ?>
                    <h3>Контактные данные</h3>
                    <?php if (!empty($profile->user->phone)): ?>
                        <b><?= Html::encode('Мобильный телефон: ') ?></b><?= Html::encode($profile->user->phone) ?><br>
                    <?php endif; ?>
                    <?php if (!empty($profile->user->email)): ?>
                        <b><?= Html::encode('Email: ') ?></b><?= Html::encode($profile->user->email) ?><br>
                    <?php endif; ?>
                    <hr>
                    <?php if (Yii::$app->user->identity && Yii::$app->user->identity->getId() == $profile->user_id): ?>
                        <h3>Места обитания <span style="font-size: 12px;">(<u>обязательно к заполнению при игре в "Мокрые войны"</u>)</span>
                        </h3>
                        <br>
                        <?php if (!empty($profile->work_address)): ?>
                            <b><?= Html::encode('Место и адрес работы:') ?></b>
                            <br><?= Html::encode($profile->work_address) ?><br>
                        <?php endif; ?>
                        <?php if (!empty($profile->study_address)): ?>
                            <b><?= Html::encode('Место и адрес учебы:') ?></b>
                            <br><?= Html::encode($profile->study_address) ?><br>
                        <?php endif; ?>
                        <?php if (!empty($profile->address)): ?>
                            <b><?= Html::encode('Адрес проживания:') ?></b><br><?= Html::encode($profile->address) ?>
                            <br>
                        <?php endif; ?>
                        <?php if (!empty($profile->schedule)): ?>
                            <b><?= Html::encode('Приблизительный распорядок дня:') ?></b>
                            <br><?= Html::encode($profile->schedule) ?><br>
                        <?php endif; ?>
                        <hr>
                        <?php if (!empty($profile->transport_mark) || !empty($profile->transport_model) || !empty($profile->transport_name)): ?>
                            <h3>Транспорт</h3>
                            <?php if (!empty($profile->transport_mark)): ?>
                                <b><?= Html::encode('Марка:') ?></b><br><?= Html::encode($profile->transport_mark) ?>
                                <br>
                            <?php endif; ?>
                            <?php if (!empty($profile->transport_model)): ?>
                                <b><?= Html::encode('Модель:') ?></b><br><?= Html::encode($profile->transport_model) ?>
                                <br>
                            <?php endif; ?>
                            <?php if (!empty($profile->transport_name)): ?>
                                <b><?= Html::encode('Гос. номер, имя:') ?></b>
                                <br><?= Html::encode($profile->transport_name) ?><br>
                            <?php endif; ?>
                            <hr>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
