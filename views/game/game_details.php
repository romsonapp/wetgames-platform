<?php

/* @var $this yii\web\View */
use app\models\Gameplay;
use app\models\GamesAuthors;
use yii\helpers\Html;

/* @var $game app\models\Games */

?>

<h2>Информация об игре</h2>
<?= \app\widgets\GameDetail\GameDetailWidget::widget(['game' => $game, 'type' => \app\widgets\GameDetail\GameDetailWidget::GAME_DETAIL]); ?>
