<?php

/* @var $this yii\web\View */
use app\models\Gameplay;
use yii\helpers\Html;

/* @var $models app\models\Games */

?>
<div class="site-index">
    <div class="panel panel-default">
        <div class="panel-heading">
            Что такое CityQuest?
        </div>
        <div class="panel-body">
            <p>В данный момент система поддерживает проведение игр формата "Мокрые войны"</p>
            <p><b>Мокрые войны</b> — захватывающая игра-экшн, где каждый сам за себя.</p>

            <p>Игра начинается с того, что каждый участник получает досье на свою жертву: фотографии, личные данные, места где жертва живёт, работает, бывает. Жертву нужно «замочить» из водяного оружия. Досье выдаются по замкнутому кругу. Каждый участник и жертва и охотник одновременно: пока участник охотится на свою жертву, кто-то охотится на него самого. Когда участнику удаётся замочить свою жертву, она отдаёт свой жизненный код, и выбывает из игры. А охотник вводит код на сайте, и получает досье на следующую жертву.</p>

            <p>Побеждает участник, который замочил наибольшее количество жертв. Победитель получает призовой фонд.</p>
        </div>
    </div>

    <h2>Запланированные игры</h2>
        <?php foreach ($models as $model):
            $authors = [];

            foreach ($model->authors as $author)
                $authors[] = $author->profile->name;

            ?>
            <div class="row">
                <?= \app\widgets\GameDetail\GameDetailWidget::widget(['game' => $model, 'type' => \app\widgets\GameDetail\GameDetailWidget::MAIN_PAGE]) ?>
            </div>
        <?php endforeach; ?>

    </div>
</div>
