<div class="site-archive">

    <h2>Архив игр</h2>
    <div class="body-content">
        <?php foreach ($models as $model):
            $authors = [];

            foreach ($model->authors as $author)
                $authors[] = $author->profile->name;

            ?>
            <div class="row">
                <?= \app\widgets\GameDetail\GameDetailWidget::widget(['game' => $model, 'type' => \app\widgets\GameDetail\GameDetailWidget::MAIN_PAGE]) ?>
            </div>
        <?php endforeach; ?>

    </div>
</div>
