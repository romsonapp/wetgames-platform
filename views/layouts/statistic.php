<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        html, body {
            background-color: #0a0a0a !important;
            color: whitesmoke !important;
        }
        .stat-table {
            width: 100%;
        }

        .stat-table tr, th, td {
            border: 1px solid black;
            position: relative;
            text-align: center !important;
            color: #0a0a0a;
            padding: 10px;
        }

        .stat-table th, .place {
            background-color: #1d5987;
            color: white;
        }

        .place {
            padding: 0px;
            width: 20px;
        }

        .victim {
            background-color: yellowgreen;
        }

        .hunter {
            background-color: green;
        }

        .killer {
            background-color: darkred;
        }

        .stat-badge {
            position: absolute;
            top: 0px;
            left: 0px;
            font-family: Tahoma;
            font-size: 12px;
            color: #004700;


            background-color: yellow;
            padding: 3px 1px 1px 1px;
            line-height: 7px;
            height: 12px;

        }
    </style>

    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>

<?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

