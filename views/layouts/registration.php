<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?php
    NavBar::begin([
        'brandLabel' => 'CityQuest',
        'options' => [
            'class' => 'navbar-inverse navbar-fixed',
        ],
    ]);
    $navItems = [
        ['label' => 'Главная', 'url' => Yii::$app->homeUrl],
        ['label' => 'Архив игр', 'url' => '/archive'],
    ];



    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => $navItems,
    ]);
    NavBar::end();


    ?>



    <?= $content ?>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left"></p>

        <p class="pull-right">Powered by <a href="http://romsonapp.com/" target="_blank" rel="nofollow">Romson
                Applications</a></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
