<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?php
    NavBar::begin([
        'brandLabel' => 'CityQuest',
        'options' => [
            'class' => 'navbar-inverse navbar-fixed',
        ],
    ]);
    $navItems = [
        ['label' => 'Главная', 'url' => Yii::$app->homeUrl],
        ['label' => 'Архив игр', 'url' => '/archive'],
    ];


    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => $navItems,
    ]);
    NavBar::end();


    ?>


    <div class="col-md-11 col-md-offset-1">
        <div class="col-md-2" style="margin-top: 10px;">
            <?php if (!Yii::$app->user->isGuest): ?>

                <div class="panel panel-default">
                    <div class="panel-heading">

                        <div class="login"
                             style="font-weight: bold; font-size: 20px; align-items: center; width: 100%;"><?= Yii::$app->user->identity->username ?></div>
                        <div class="fullname"><?= Yii::$app->user->identity->profile->name ?></div>

                    </div>
                    <div class="panel-body">
                        <p><?= Html::a('Профиль', Yii::$app->urlManager->createUrl(['/user/profile'])); ?></p>
                        <div
                            class="logout text-center"><?= Html::a('Выход', ['/user/logout'], ['data' => ['method' => 'post']]); ?></div>


                    </div>
                </div>
            <?php else: ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?php \dektrium\user\widgets\Login::begin(); ?>
                        <?php \dektrium\user\widgets\Login::end(); ?>
                        <div class="text-center"><?= Html::a('Регистрация', ['user/register']); ?></div>
                    </div>
                </div>

            <?php endif; ?>

            <?php if (Yii::$app->user->identity && (Yii::$app->user->identity->isAuthor() || Yii::$app->user->identity->getIsAdmin())): ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <b>Админка</b>
                    </div>
                    <div class="panel-body">
                        <p><?= Html::a('Управление играми', Yii::$app->urlManager->createUrl(['admin/games'])); ?></p>
                        <?php if (Yii::$app->user->identity->getIsAdmin()): ?>
                            <p><?= Html::a('Управление пользователями', Yii::$app->urlManager->createUrl(['admin/user'])); ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>

        </div>


        <div class="col-md-7"
             style="margin: 10px;">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
        <div class="col-md-2" style="margin-top: 10px;"></div>

    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left"></p>

        <p class="pull-right">Powered by <a href="http://romsonapp.com/" target="_blank" rel="nofollow">Romson
                Applications</a></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
