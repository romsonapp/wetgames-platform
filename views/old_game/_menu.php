<?php
use yii\helpers\Html;

?>
<?= Html::a('Изменить настройки игры', ['update', 'id' => $model->game_id], ['class' => 'btn btn-primary']) ?>
<?= Html::a('Удалить игру', ['delete', 'id' => $model->game_id], [
    'class' => 'btn btn-danger',
    'data' => [
        'confirm' => 'Вы уверены а том, что хотите удалить игру?',
        'method' => 'post',
    ],
]) ?>
<?= Html::a('Управление игроками', ['players-controll', 'game' => $model->game_id], ['class' => 'btn btn-success']) ?>

