<?php
use yii\helpers\Html;

?>
<?= Html::a('Назад к настройкам игры', ['view', 'id' => $model->game_id], ['class' => 'btn btn-primary']) ?>

<div class="row">
    <div class="col-md-12">
        <div class="col-sm-7">

        </div>
    </div>
</div>
<h1>Управление игроками</h1>
<hr>
<div class="col-md-12">

    <div class="col-md-6">

        <ul>
            <h3>Нужно подтверждение</h3>
            <?php foreach (\app\models\Gameplay::getInvitedPlayers($model->gameplay_id) as $player): ?>
                <li style="list-style: none; padding-bottom: 25px;">
                    <h4>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <?= $player->user->profile->name ?>
                            </div>
                            <div class="col-md-1">
                                <?= "<a href='/game/player-set-active?game=" . $model->game_id . '&user_id=' . $player->user_id . "'><div style='color: #00aa00;' class='glyphicon glyphicon-ok'></div></a>" ?>
                            </div>
                        </div>
                    </h4>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="col-md-6">
        <ul>
            <h3>Подтвержденные</h3>
            <?php foreach (\app\models\Gameplay::getActivePlayers($model->gameplay_id) as $player): ?>
                <li style="list-style: none; padding-bottom: 25px;">
                    <h4>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <?= $player->user->profile->name ?>
                            </div>
                            <div class="col-md-1">
                                <?= "<a href='/game/player-set-inactive?game=" . $model->game_id . '&user_id=' . $player->user_id . "'><div style='color: #9e0505;' class='glyphicon glyphicon-remove'></div></a>" ?>
                            </div>
                        </div>
                    </h4>
                </li>
            <?php endforeach; ?>
        </ul>

    </div>

</div>