<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Games */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="games-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'game_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'game_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'game_start')->textInput() ?>

    <?= $form->field($model, 'game_stop')->textInput() ?>

    <?= $form->field($model, 'fee')->textInput()->input('number') ?>

    <?= $form->field($model, 'online_statistic')->radioList(['Включить', 'Отключить']) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <script>

        $('#games-game_start').daterangepicker({
            "singleDatePicker": true,
            "showWeekNumbers": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "timePickerIncrement": 1,
            "autoApply": true,
            "drops": "up",
            "dateLimit": {
                "days": 7
            },
            "locale": {
                "format": 'MM/DD/YYYY HH:mm'
            }
        });
        $('#games-game_stop').daterangepicker({
            "singleDatePicker": true,
            "showWeekNumbers": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "timePickerIncrement": 15,
            "autoApply": true,
            "drops": "up",
            "dateLimit": {
                "days": 7
            },
            "locale": {
                "format": 'MM/DD/YYYY HH:mm'
            }
        });


    </script>

</div>
