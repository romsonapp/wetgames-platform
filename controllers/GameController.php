<?php
namespace app\controllers;

use app\models\Gameplay;
use app\models\Games;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class GameController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['invite'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'invite' => ['post'],
                    'cancel-invite' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex($id)
    {
        $game = Games::find()->where(['game_id' => $id])->one();

        return $this->render('game_details', compact('game', 'p_invited_count', 'p_played_count'));
    }

    public function actionInvite($id) {
            $game = Games::find()->where(['game_id' => $id])->one();
            $game->invite(Yii::$app->getUser()->id);

            return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionCancelInvite($id) {
        $game = Games::find()->where(['game_id' => $id])->one();
        $game->cancelInvite(Yii::$app->getUser()->id);

        return $this->redirect(Yii::$app->request->referrer);
    }
}