<?php

namespace app\controllers;

use app\models\Gameplay;
use app\models\Games;
use dektrium\user\models\User;
use Yii;
use yii\web\Controller;

class GoController extends Controller
{
    /**
     * @param $game_id
     * @return string|\yii\web\Response
     */
    public function actionIndex($game_id)
    {
        $this->layout = 'go-layout_theme_1';

        $game = Games::find()->where(['game_id' => $game_id])->one();
        $time = time() - strtotime($game->game_start);
        if ($time < 0) {
            return $this->render('early', ['start_time' => $game->game_start]);
        }

        if(!Yii::$app->user->identity || !$game->isInvited() || !$game->canPlayerEntry() || !$game->hasTarget()) {
            return $this->render('no-access');
        }

/*
        if(!Yii::$app->user->identity && !$game->getGameplay()->find()->where(['user_id' => Yii::$app->user->identity->getId()])->exists() && $game->getGameplay()->find()->where(['user_id' => Yii::$app->user->identity->getId(), 'status' => Gameplay::PLAYER_INVITED])->exists()) {
            return $this->render('no-access');
        }*/

        if ($game->getGameplay()->checkWinner() || (time() - strtotime($game->game_stop)) >= 0) {
            return $this->render('win');
        }

        $model = $game->getGameplay()->find()->where(['user_id' => Yii::$app->user->identity->getId()])->one();

        $target_id = $model->target_id;
        $profile = User::find()->where(['id' => $target_id])->one();

        if (Yii::$app->request->isPost) {

            $lifecode = strtolower(trim(str_replace('-', '', Yii::$app->request->post('lifecode'))));

            $target = $game->getGameplay()->find()->where(['lifecode' => $lifecode])->one();

            if ($target && $target->user_id == $model->target_id) {

                if (strpos($target->lifecode, $lifecode) === 0) {

                    $new_target_id = $target->target_id;

                    if ($target->kill()) {
                        $model->target_id = $new_target_id;

                        if ($model->update()) {
                            $model->refresh();
                            $profile->refresh();
                            return $this->refresh();

                        } else {
                            //TODO: Обработать ошибку и отправить репорт

                        }

                    } else {


                    }
                }
            }else{

                Yii::$app->session->setFlash('error', 'Код жизни неверный или не вашей жертвы');
                return $this->refresh();
            }
        }


        return $this->render('gameplay', [
            'profile' => $profile,
            'model' => $model
        ]);
    }

}
