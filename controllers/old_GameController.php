<?php

namespace app\controllers;

use app\models\Gameplay;
use app\models\GamesAuthors;
use app\models\User;
use dektrium\user\filters\AccessRule;
use dektrium\user\models\Profile;
use Yii;
use app\models\Games;
use app\models\GamesSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GameController implements the CRUD actions for Games model.
 */
class GameController extends Controller
{

}
