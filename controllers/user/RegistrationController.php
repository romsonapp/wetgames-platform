<?php
namespace app\controllers\user;

use app\models\user\RegistrationForm;

class RegistrationController extends \dektrium\user\controllers\RegistrationController
{
    public function getViewPath()
    {
        return '@app/views/user/registration';
    }

    public function actionRegister()
    {
        $this->layout = '@app/views/layouts/registration';
        if (!$this->module->enableRegistration) {
            throw new NotFoundHttpException();
        }

        /** @var RegistrationForm $model */
        $model = \Yii::createObject(RegistrationForm::className());
        $event = $this->getFormEvent($model);

        $this->trigger(self::EVENT_BEFORE_REGISTER, $event);

        $this->performAjaxValidation($model);

        if ($model->load(\Yii::$app->request->post()) && $model->register()) {
            $this->trigger(self::EVENT_AFTER_REGISTER, $event);

            $meta = [
                'http-equiv' => 'Refresh',
                'content' => '3; url=/',
            ];
            \Yii::$app->view->registerMetaTag($meta);
            return $this->render('../message', [
                'title'  => \Yii::t('user', 'Your account has been created'),
                'module' => $this->module,
            ]);
        }

        return $this->render('register', [
            'model'  => $model,
            'module' => $this->module,
        ]);
    }
}
