<?php
namespace app\controllers\user;



use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class SecurityController extends \dektrium\user\controllers\SecurityController  {
    public function getViewPath()
    {
        return '@app/views/user/security';
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'actions' => ['login', 'auth'], 'roles' => ['?']],
                    ['allow' => true, 'actions' => ['login', 'auth', 'logout'], 'roles' => ['@']],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


}