<?php
namespace app\controllers;

use dektrium\user\controllers\ProfileController;
use yii\base\Controller;
use yii\web\NotFoundHttpException;

class UserController extends ProfileController  {
    /**
     * Shows user's profile.
     *
     * @param int $id
     *
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionShow($id)
    {
        $profile = $this->finder->findProfileById($id);

        if ($profile === null) {
            throw new NotFoundHttpException();
        }

        return $this->render('show', [
            'profile' => $profile,
        ]);
    }
}