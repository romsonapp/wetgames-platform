<?php

namespace app\controllers;


use app\models\Games;
use app\models\Monitor;
use yii\web\Controller;

class StatisticController extends Controller
{
    public function actionIndex($game)
    {
        $this->layout = 'statistic';

        return $this->render('index', ['game' => $game]);
    }
}